﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Net;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
//using SchumiLibrary;
//using SchumiLibrary.Services;
using ConfigCopier;
using ConfigCopier.Forms;
using ConfigCopier.Services;

namespace ConfigCopier
{
    public class MyTrayIconContainer : ApplicationContext
    {

        private NotifyIcon _MyTrayIcon;
        private Boolean _AdminEnabled = false;

        public Boolean CriticalError = false;
        //private string _IconPath = ".\\TrayAppResources\\TrayApp.ico";

        private FormSelectNick MyFormSelectNick;
        private FormAutomaticConfigCopier MyFormAutomaticConfigCopier;
        private FormConfigInspector MyFormConfigInspector;

        private BackgroundWorker _bckWorker = new BackgroundWorker();

        // Constructor
        public MyTrayIconContainer()
        {
            _MyTrayIcon = new NotifyIcon();

            try
            {
                _AdminEnabled = ConfigurationManager.AppSettings["debugButton"].ToUpper() == "TRUE";

                try
                {
                    //Icon ico = new Icon(_IconPath);
                    // Get the icon from its own exe file
                    Icon ico = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
                    _MyTrayIcon.Icon = ico;
                }
                catch (Exception e)
                {
                    MessageBox.Show("Start error : TrayIcon init : " + e.Message);
                }
                _MyTrayIcon.Text = "ConfigCopier";
                _MyTrayIcon.Visible = true;

                // init backgroundwerker for server polling
                //mandatory. Otherwise will throw an exception when calling ReportProgress method  
                _bckWorker.WorkerReportsProgress = true;
                //mandatory. Otherwise we would get an InvalidOperationException when trying to cancel the operation  
                _bckWorker.WorkerSupportsCancellation = true;
                //Add the event handler to the BackgroundWorker instance's events
                _bckWorker.DoWork += new DoWorkEventHandler(bw_DoWork);                

                // Adds menu and items ...
                InitTrayIcon();

                // open select nick window automaticly
                OpenSelectNickDialog();
                
            }
            catch (Exception e)
            {
                MessageBox.Show("App failed to initialise : " + e.Message);
                ApplicationExit();
            }
        }

        // Exit disposals
        private void ApplicationExit()
        {
            CriticalError = true;
            ExitThread();
            if (_MyTrayIcon != null)
            {
                _MyTrayIcon.Dispose();
            }
        }

        // BackgroundWorker
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if ((worker.CancellationPending == true))
            {
                e.Cancel = true;
            }
            else
            {
                // Perform a time consuming operation and report progress.
                //PollServerOnce(1);
                
                System.Threading.Thread.Sleep(60000); //ms
                //worker.ReportProgress((i * 10));
            }
        }

        public void StartBackgroundWorker(int usrId)
        {
            //Start running the background operation by calling the RunWorkerAsync method
            if (_bckWorker.IsBusy != true)
            {
                _bckWorker.RunWorkerAsync();
            }            
        }

        public void CancelBackgroundWorker()
        {
            //Cancel the background operation by calling the CancelAsync method
            if (_bckWorker.WorkerSupportsCancellation == true)
            {
                _bckWorker.CancelAsync();
            }
        }


        // add menu items here
        public void InitTrayIcon()
        {

            ContextMenu cmm = new ContextMenu();

            MenuItem mi = new MenuItem("Select NickName", cmm_SelectNick_Click);
            cmm.MenuItems.Add(mi);

            //Separator
            mi = new MenuItem("-");
            cmm.MenuItems.Add(mi);

            if (_AdminEnabled)
            {
                MenuItem misubMenuItem = cmm.MenuItems.Add("Actions");

                mi = new MenuItem("Config Explorer", cmm_ConfigExplorer_Click);
                misubMenuItem.MenuItems.Add(mi);

                misubMenuItem = cmm.MenuItems.Add("StartUp");

                mi = new MenuItem("Explorer", cmm_Explorer_Click);
                misubMenuItem.MenuItems.Add(mi);

                List<String> startUpItems = new List<string>();

                startUpItems = SchumiLibrary.Services.ConfigService.GetConfigValuesWithKeyLike("StartUp");

                foreach (String startUpItem in startUpItems)
                {
                    mi = new MenuItem(startUpItem, cmm_ExecuteMenuItemString_Click);
                    misubMenuItem.MenuItems.Add(mi);
                }


                //misubMenuItem = cmm.MenuItems.Add("Proxy");
                //    mi = new MenuItem("Proxy enabled", cmm_ProxyEnabled_Click);
                //    bool proxyEnabled = TrayService.GetProxyEnabled();
                //    if(proxyEnabled)
                //    {
                //        mi.Checked = true;
                //    }
                //    misubMenuItem.MenuItems.Add(mi);
            }
                

            mi = new MenuItem("Exit", cmm_Exit_Click);
            cmm.MenuItems.Add(mi);

            _MyTrayIcon.ContextMenu = cmm;

            _MyTrayIcon.ContextMenu.Popup += cmm_PopUp;

        }

        private void OpenSelectNickDialog()
        {
            MyFormSelectNick = new FormSelectNick();
            MyFormSelectNick.ShowDialog();
            //MyFormSelectNick.Close();
        }

        private void cmm_PopUp(object sender, EventArgs e)
        {
 
        }
        
        private void cmm_SelectNick_Click(object sender, EventArgs e)
        {
            if (MyFormSelectNick != null && MyFormSelectNick.Modal == false)
                OpenSelectNickDialog();
        }

        private void cmm_ConfigExplorer_Click(object sender, EventArgs e)
        {
            MyFormConfigInspector = new FormConfigInspector();
            MyFormConfigInspector.ShowDialog();
        }

        private void cmm_Explorer_Click(object sender, EventArgs e)
        {
            SchumiLibrary.Services.SystemService.ExecuteCommand("explorer.exe", "");
        }

        private void cmm_ExecuteMenuItemString_Click(object sender, EventArgs e)
        {
            string menuString = ((MenuItem)sender).Text;
            try
            {
                var sep = new string[] { "$$" };
                var arr = menuString.Split(sep, StringSplitOptions.None);

                var arg0 = arr[0];
                var arg1 = arr[1];

                SchumiLibrary.Services.SystemService.ExecuteCommand(arg0, arg1);
            }
            catch (Exception E)
            {

                MessageBox.Show(E.Message);
            }
        }

        private void cmm_Exit_Click(object sender, EventArgs e)
        {
            foreach (Form frm in Application.OpenForms)
            {
                frm.Close();
            }
            
            ApplicationExit();
        }

        private bool PollServerOnce(int userId)
        {
            //http://192.168.1.2/interface/usrLogin.php?usrId=xxx&update=true

            var timerFrequency = ConfigurationManager.AppSettings["LoginNotifyUrlFrequency"];
            var url = ConfigurationManager.AppSettings["PollServerUrl"];  // no remote configured ??
            url += "?usrid=" + userId + "&update=true";

            try
            {

                //library and HttpWebRequest HttpWebResponse classes
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //set proxy : no proxy
                    request.Proxy = null;

                var resp = request.GetResponse();
                //Stream stream = resp.GetResponseStream();
                //StreamReader reader = new StreamReader(stream);
                //MessageBox.Show(reader.ReadToEnd());

                //Response.Write(reader.ReadToEnd);
            }
            catch (Exception e)
            {
                //Add error logging here !            
                return false;
            }
            return true;
        }


        /* thread example
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace SL_BackgroundWorker_CS
{
    public partial class Page : UserControl
    {
        private BackgroundWorker bw = new BackgroundWorker();

        public Page()
        {
            InitializeComponent();

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
        }
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (bw.IsBusy != true)
            {
                bw.RunWorkerAsync();
            }
        }
        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (bw.WorkerSupportsCancellation == true)
            {
                bw.CancelAsync();
            }
        }
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            for (int i = 1; (i <= 10); i++)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(500);
                    worker.ReportProgress((i * 10));
                }
            }
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                this.tbProgress.Text = "Canceled!";
            }

            else if (!(e.Error == null))
            {
                this.tbProgress.Text = ("Error: " + e.Error.Message);
            }

            else
            {
                this.tbProgress.Text = "Done!";
            }
        }
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.tbProgress.Text = (e.ProgressPercentage.ToString() + "%");
        }
    }
}

         */
    

    }
}