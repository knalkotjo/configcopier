﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using ConfigCopier.Forms;
using ConfigCopier.Models;
using ConfigCopier.Services;
using File = System.IO.File;

namespace ConfigCopier
{
    public partial class FormAutomaticConfigCopier : Form
    {

        //////////////////////////////////////////////////////////////////////////////////////
        // Constants
        //////////////////////////////////////////////////////////////////////////////////////
        private const string modeBackup = "backup";
        private const string modeRestore = "restore";
        private string defaultFtpPassword = "userftp";

        //////////////////////////////////////////////////////////////////////////////////////
        // members
        //////////////////////////////////////////////////////////////////////////////////////
        
        private NickDataModel _NickName;

        private bool _IncludeUserConfigForRestore = false;
        private List<Models.NickDataModel> _NickList;

        private ConfigCopierData _MyConfigCopierData = new ConfigCopierData();

        private string _FtpServer = "";
        private string _FtpUser = "";
        private string _FtpPassword = "";

        private bool _RunRemote = false;

        public FormAutomaticConfigCopier(NickDataModel nickName, List<Models.NickDataModel> nickList, string mode)
        {
            InitializeComponent();

            // HC disable backup
            //buttonBackup.Enabled = false;

            //_IncludeUserConfigForRestore = false; // default setting hardcoded 
            _IncludeUserConfigForRestore = true; // default setting hardcoded 
            checkBoxIncludeUserConfig.Checked = _IncludeUserConfigForRestore;

            // Read settings from App.Config
            _MyConfigCopierData.ReadConfigSettings();


            // Get Nick List
            // get nicklist data
            _NickList = nickList;
            _NickName = nickName;


            // Init program, some startup tasks, action buton security
            Initialize();

            // to prevent selected text in the edits
            textBoxFocusser.Select();


            if (mode == modeRestore)
            {
                StartCopy(modeRestore);
            }
            else if (mode == modeBackup)
            {
                StartCopy(modeBackup);
            }

        }

        private void Initialize()
        {
            textBoxProgress.Text = "";
            UpdateProgressBar(0);
            if (_NickName != null)
            {
                labelSelectedNickNameValue.Text = _NickName.NickName + " (" + _NickName.UserId + ")";
            }
            else
            {
                labelSelectedNickNameValue.Text = "NO NICKNAME !!";
            }
            SetActionButtonsSecurity();
            // not used anymore ?
            //buttonSelectNickName.Enabled = true;

            _RunRemote = ConfigurationManager.AppSettings["RunParameterRemote"].ToUpper() == "TRUE";
            _FtpServer = _RunRemote ? ConfigurationManager.AppSettings["ftpServerRemote"] : ConfigurationManager.AppSettings["ftpServer"];
            _FtpUser = ConfigurationManager.AppSettings["ftpUser"];
            _FtpPassword = ConfigurationManager.AppSettings["ftpPassword"];
            if (String.IsNullOrEmpty(_FtpPassword)) _FtpPassword = defaultFtpPassword;

        }

        private void SetActionButtonsSecurity()
        {
            bool val = false;
            if (_NickName != null && _NickName.UserId != 0)
            {
                val = true;
            }
            buttonBackup.Enabled = val;
            buttonRestore.Enabled = val;
        }


        private void StartCopy(string mode)
        {
            buttonSelectNickName.Enabled = false;
            textBoxProgress.Text = "";
            UpdateProgressBar(0);
            AddProgressLine("Start ...");
            CheckCreateDirs();
            UpdateProgressBar(25);
            if (mode == modeBackup)
            {
                StartCopyBackup();
            }
            else
            {
                StartCopyRestore();
            }
            AddProgressLine("End ...");
            buttonSelectNickName.Enabled = true;
            UpdateProgressBar(100);


        }

        private void StartCopyRestore()
        {
            //if (GetRestoreConfirmation())
            //{
                AddProgressLine("Restoring config for " + _NickName.NickName);

                DeleteCurrentLocalConfigFiles();

                CopyAllCfgFilesFromFtp();

                UpdateProgressBar(95);
                AddProgressLine("Config for " + _NickName.NickName + " restored from server");
            //}
            //else
            //{
            //    AddProgressLine("Config restore cancelled by user");
            //}
        }
        private void DeleteCurrentLocalConfigFiles()
        {
            String cfgDir = _MyConfigCopierData._HalfLifeConfigPath;
            // list all *.cfg files
            List<FileInfo> listCfgFiles = SchumiLibrary.Services.SystemService.GetAllFilesInDir(cfgDir, "cfg");
            // delete all files in list
            foreach (FileInfo fileInfo in listCfgFiles)
            {
                string path = fileInfo.FullName;
                AddProgressLine("Deleting " + path);
                DeleteFile(path);
            }
        }
        private void CopyAllCfgFilesFromFtp() 
        { 
            //copy all cfg files from ftp // filter ?
            String cfgDir = _MyConfigCopierData._HalfLifeConfigPath;
            ConfigCopierService.getDirFilesViaFtp(_FtpServer, _FtpUser, _FtpPassword, _NickName.UserId.ToString(),cfgDir, ".cfg");
        }
        
        private void StartCopyBackup()
        {
            // check dir on ftpserver
            if (CheckDirOnFtpServer())
            {
                // rename all *.cfg onserver
                RenameAllCfgOnFtpServer();
            }
            else 
            {
                CreateFtpServerDir();
            }
 
            // check if there are local files to copy


            if (CheckIfLocalFilesForBackup())
            {
                // copy all *.cfg to server
                CopyAllCfgToFtpServer();

                UpdateProgressBar(95);
                AddProgressLine("Backup config for " + _NickName.NickName + " done !");
            }
            else
            {
                UpdateProgressBar(95);
                AddProgressLine("Backup config for " + _NickName.NickName + " : no files to backup !");
            }
        }
        private bool CheckDirOnFtpServer()
        {
          // bool dirExists = ConfigCopierService.Di(_FtpServer, _FtpUser, _FtpPassword, _NickName.UserId.ToString(), cfgDir, ".cfg");


            return false;
        }
        private void CreateFtpServerDir()
        {
        
        }
        private void RenameAllCfgOnFtpServer()
        {
            // not *.cfg !!


        }
        private bool CheckIfLocalFilesForBackup()
        {
            //todo : check this

            return true;
        }
        private void CopyAllCfgToFtpServer()
        {
    
        }

               
        private void InternalCheckCreateDir(String path)
        {
            // do this with a delegate
            //ConfigCopierService.CheckCreateDir(path, AddProgressLine);

            // Simple
            if (ConfigCopierService.CheckDirectoryPath(Path.GetPathRoot(path)))
            { 
                AddProgressLine("Checking/Creating directory " + path);
                if (ConfigCopierService.CheckCreateDir(path))
                {
                    AddProgressLine("[" + path + "] created");
                }
                else
                {
                    AddProgressLine("[" + path + "] allready exists");
                }
            }
            else
            {
                AddProgressLine("Can not access mapped drive [" + Path.GetPathRoot(path) + "]");
            }
        }
        private void CheckCreateDirs()
        {

            // Check create halflife program dir
            InternalCheckCreateDir(_MyConfigCopierData._HalfLifeConfigPath);
            UpdateProgressBar(16);
        }

        private void DeleteFile(string fileName)
        {
            if(File.Exists(fileName))
            {
                File.Delete(fileName);
                AddProgressLine("[" + fileName + "] Deleted !");
            }
            else
            {
                AddProgressLine("[" + fileName + "] does not exist : not deleted ...");
            }
        }

        private void AddProgressLine(String message)
        {
            textBoxProgress.Text += message + Environment.NewLine;
        }

        private void UpdateProgressBar(int percentage)
        {
            if (percentage > 101)
            {
                progressBarCopy.Value = 100;
            }
            else if (percentage < 0)
            {
                progressBarCopy.Value = 0;
            }
            else
            {
                progressBarCopy.Value = percentage;
            }
            labelProgressPercentage.Text = " " + percentage + " %";
        }

        
        private void buttonSelectNickName_Click(object sender, EventArgs e)
        {

            //this.Close();            
            
            //// create instance of the select nick form
            //var selectNickForm = new FormSelectNick();
            //selectNickForm.ShowDialog();
            //// on selected get valuse from this form var
            //if(selectNickForm._SelectedNickName != null)
            //{
            //    SetNickFromSelectNickForm(selectNickForm._SelectedNickName);
            //}
            //else
            //{
            //    MessageBox.Show("No nickname selected !", "Select nickname");
            //}

            //selectNickForm.Dispose();
        }

        private void buttonOpenConfigInspector_Click(object sender, EventArgs e)
        {
            var inspForm = new FormConfigInspector();
            inspForm.ShowDialog();
        }

        
        private void buttonBackup_Click(object sender, EventArgs e)
        {
            StartCopy("BACKUP");
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            StartCopy("RESTORE");
        }

        private void checkBoxIncludeUserConfig_CheckedChanged(object sender, EventArgs e)
        {
            _IncludeUserConfigForRestore = checkBoxIncludeUserConfig.Checked;
        }

        
        private void buttonAddToStartup_Click(object sender, EventArgs e)
        {
            // add to startup
            //HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run

            //string startUpPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            //string me = Application.ExecutablePath;

            //string addToStartUpServerPath = ConfigurationSettings.AppSettings["AddToStartUpServerPath"].ToString();
            //string addToStartUpClientPath = ConfigurationSettings.AppSettings["AddToStartUpClientPath"].ToString();

            //if (String.IsNullOrEmpty(addToStartUpClientPath))
            //{
            //    startUpPath = addToStartUpClientPath;
            //}
            //if (String.IsNullOrEmpty(addToStartUpServerPath))
            //{
            //    me = addToStartUpServerPath;
            //}

            //try
            //{
            //    WshShellClass WshShell = new WshShellClass();
            //    IWshShortcut MyStartUpShortcut;
            //    MyStartUpShortcut = (IWshShortcut)WshShell.CreateShortcut(startUpPath + "\\ConfigCopier.lnk");
            //    MyStartUpShortcut.TargetPath = me;
            //    MyStartUpShortcut.Description = "Shortcut to " + MyStartUpShortcut.TargetPath;
            //    MyStartUpShortcut.Save();

            //    MessageBox.Show("ConfigCopier successfully added to your startup folder");

            //}
            //catch (Exception Ex)
            //{

            //    MessageBox.Show("Exception on adding to startup : " + Ex.Message);
            //}

        }

        private void buttonAddToDesktop_Click(object sender, EventArgs e)
        {
            //string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //string me = Application.ExecutablePath;

            //string addToDesktopServerPath = ConfigurationSettings.AppSettings["AddToDesktopServerPath"].ToString();
            //string addToDesktopClientPath = ConfigurationSettings.AppSettings["AddToDesktopClientPath"].ToString();

            //if (String.IsNullOrEmpty(addToDesktopClientPath))
            //{
            //    desktopPath = addToDesktopClientPath;
            //}
            //if (String.IsNullOrEmpty(addToDesktopServerPath))
            //{
            //    me = addToDesktopServerPath;
            //}

            //try
            //{
            //    WshShellClass WshShell = new WshShellClass();
            //    IWshShortcut MyDesktopShortcut;
            //    MyDesktopShortcut = (IWshShortcut)WshShell.CreateShortcut(desktopPath + "\\ConfigCopier.lnk");
            //    MyDesktopShortcut.TargetPath = me;
            //    MyDesktopShortcut.Description = "Shortcut to " + MyDesktopShortcut.TargetPath;
            //    MyDesktopShortcut.Save();

            //    MessageBox.Show("ConfigCopier successfully added to your desktop");

            //}
            //catch (Exception Ex)
            //{

            //    MessageBox.Show("Exception on adding to desktop: " + Ex.Message);
            //}
        }

        private void FormAutomaticConfigCopier_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        private void FormAutomaticConfigCopier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        //private bool GetRestoreConfirmation()
        //{

        //    //if(MessageBox.Show("You want to fetch the backup for ") " and copy it locally ?") == System.Windows.Forms.DialogResult.OK)

        //    var dlgRes = MessageBox.Show(
        //        "Are you sure ?" + Environment.NewLine +"This will restore the config for " + _NickName.NickName + " on this machine !", 
        //        "Confirm Config Restore", 
        //        MessageBoxButtons.YesNo, 
        //        MessageBoxIcon.Question);
        //    return (dlgRes == DialogResult.Yes);
        //}

        //private bool GetBackupConfirmation()
        //{

        //    //if(MessageBox.Show("You want to fetch the backup for ") " and copy it locally ?") == System.Windows.Forms.DialogResult.OK)

        //    var dlgRes = MessageBox.Show(
        //        "Are you sure you want to backup the config for " + _NickName.NickName + " ?",
        //        "Confirm Config Backup",
        //        MessageBoxButtons.YesNo,
        //        MessageBoxIcon.Question);
        //    return (dlgRes == DialogResult.Yes);
        //}
    //private void CopyWithProgress(FileCopyInfo copyInfo, int progressBegin, int progressEnd)
        //{
        //    AddProgressLine("Copying [" + copyInfo.Source + "] to [" + copyInfo.Destination +
        //        "] (Overwrite=" + copyInfo.Overwrite + ")");

        //    // this makes the old way still work : only copy valids (if dir and file is valid!)
        //    if (copyInfo.IsValid() || copyInfo.Destination.Contains("cstrike"))
        //    {
        //        // file copy
        //        //File.Copy(copyInfo.Source, copyInfo.Destination, copyInfo.Overwrite);

        //        //ftp
        //        //var ftpPath = ConfigurationManager.AppSettings["ftpPath"];
        //        ConfigCopierService.getFileViaFtp(_FtpServer, _FtpUser, _FtpPassword, _NickName.UserId.ToString(), copyInfo.Source, copyInfo.Destination);
                
        //        AddProgressLine("** ++ ** Copy done !");
        //    }
        //    else
        //    {
        //        AddProgressLine("** -- ** Not able to copy : " + copyInfo.NotValidErrorMessage);
        //    }
        //    UpdateProgressBar(progressBarCopy.Value + 5);
        //}

    }
}
