﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ConfigCopier.Models;
using ConfigCopier.Services;

namespace ConfigCopier.Forms
{
    /**************************************************
        TODO :
     
      - Buttons to copy - overwrite - rename
     
     
     *************************************************/
    
    public partial class FormConfigInspector : Form
    {
        private ConfigCopierData _MyConfigCopierData = new ConfigCopierData();
        private List<Models.NickDataModel> _NickList;
        private int TestMode = 0;

        private string originalHalfLifeConfigPath;
        private string originalLocalConfigPath;
        private string originalServerConfigPath;  

        public FormConfigInspector()
        {
            InitializeComponent();

            toolStripButtonToggleTestMode.Enabled = false;

            // Read settings from App.Config
            _MyConfigCopierData.ReadConfigSettings();
            // get nicklist data : test : todo, get actual data
            _NickList =  ConfigCopierService.GetNickListTestData();

            InitializeUi(TestMode);
        }

        private void InitializeUi(int testMode)
        {
            // test mode : put the test dirs in the production properties
            if (testMode == 1)
            {
                if (String.IsNullOrEmpty(originalHalfLifeConfigPath))
                {
                    originalHalfLifeConfigPath = _MyConfigCopierData._HalfLifeConfigPath;
                    originalLocalConfigPath = _MyConfigCopierData._LocalConfigPath;
                    originalServerConfigPath = _MyConfigCopierData._ServerConfigPath;
                }

                this.Text = "   ***TEST***  " + this.Text + "  ***TEST***";
                _MyConfigCopierData._HalfLifeConfigPath = _MyConfigCopierData._HalfLifeConfigPathTest;
                _MyConfigCopierData._LocalConfigPath = _MyConfigCopierData._LocalConfigPathTest;
                _MyConfigCopierData._ServerConfigPath = _MyConfigCopierData._ServerConfigPathTest;
            }
            else
            {
                this.Text = "Config Inspector";
                if (!String.IsNullOrEmpty(originalHalfLifeConfigPath))
                {
                    _MyConfigCopierData._HalfLifeConfigPath = originalHalfLifeConfigPath;
                    _MyConfigCopierData._LocalConfigPath = originalLocalConfigPath;
                    _MyConfigCopierData._ServerConfigPath = originalServerConfigPath;
                }
            }

            labelLocalHalflifeFolder.Text = "Local HalfLife Folder";
            textBoxLocalHalfLifeFolder.Text = _MyConfigCopierData._HalfLifeConfigPath;
            labelLocalBackupFolder.Text = "Local Backup Folder";
            textBoxLocalBackupFolder.Text = _MyConfigCopierData._LocalConfigPath;
            labelServerBackupFolder.Text = "Server Config Folder";
            textBoxServerBackupFolder.Text = _MyConfigCopierData._ServerConfigPath;

            checkBoxFilter1.Text = "Filter on " + _MyConfigCopierData._FileNameExt;

            var currentConfigFile = _MyConfigCopierData._HalfLifeConfigPath + "\\" + 
                                    _MyConfigCopierData._FileNameConfig + 
                                    _MyConfigCopierData._FileNameExt;

            var currentNickName = ConfigCopierService.GetConfigFileNickName(currentConfigFile);
            toolStripMain.Items[0].Text = "NickName : " + currentNickName;

            RefreshData();

        }

        private void RefreshData()
        {
            listViewLocalHalfLifeFolder.Items.Clear();
            listViewLocalBackupFolder.Items.Clear();
            listViewServerBackupFolder.Items.Clear();

            try
            {
                RefreshFileList(_MyConfigCopierData._HalfLifeConfigPath, listViewLocalHalfLifeFolder);
                RefreshFileList(_MyConfigCopierData._LocalConfigPath, listViewLocalBackupFolder);
                RefreshFileList(_MyConfigCopierData._ServerConfigPath, listViewServerBackupFolder);
            }
            catch (Exception E)
            {

                MessageBox.Show(E.Message);
            }
        }

        private void RefreshFileList(String dirPath,ListView lv)
        {
            DirectoryInfo theFolder = new DirectoryInfo(dirPath);
            if (!theFolder.Exists)
            {
                throw new DirectoryNotFoundException("Folder not found: " + dirPath);
            }

            // Clear the list
            lv.Items.Clear();

            // Add the upDir item (..) as first item, unless its the original folder
            if (lv == listViewServerBackupFolder || lv == listViewLocalBackupFolder)
            {
                if (lv.Items.Count == 0)
                {
                    String textBoxText;
                    String original;
                    if (lv == listViewServerBackupFolder)
                    {
                        textBoxText = textBoxServerBackupFolder.Text;
                        if (TestMode == 1)
                            original = _MyConfigCopierData._ServerConfigPathTest;
                        else
                            original = originalServerConfigPath;
                    }
                    else
                    {
                        textBoxText = textBoxLocalBackupFolder.Text;
                        if (TestMode == 1)
                            original = _MyConfigCopierData._LocalConfigPathTest;
                        else
                            original = originalLocalConfigPath;
                    }

                    if (textBoxText != original)
                    {
                        var lvItem = lv.Items.Add("..");
                        lvItem.BackColor = Color.AliceBlue;
                        lv.Items[lvItem.Index].SubItems.Add("back");
                    }
                }
            }

            // list all subfolders in folder
            foreach (DirectoryInfo nextFolder in theFolder.GetDirectories())
            {
                //var nickName = nextFolder.Name.Substring(0, 1);
                var nickName = GetDirectoryNickName(nextFolder.Name);
                var lvItem = lv.Items.Add(nextFolder.Name);
                lvItem.BackColor = Color.Gold;
                lv.Items[lvItem.Index].SubItems.Add(nickName);
            }
            // list all files in folder
            foreach (FileInfo nextFile in theFolder.GetFiles())
            {
                bool includeFile = true;
                if (checkBoxFilter1.Checked)
                {
                    if (nextFile.Extension != _MyConfigCopierData._FileNameExt)
                    {
                        includeFile = false;
                    }
                }
                if(includeFile)
                {
                    var nickName = ConfigCopierService.GetConfigFileNickName(dirPath + "\\" + nextFile.Name);
                    var lvItem = lv.Items.Add(nextFile.Name);
                    lv.Items[lvItem.Index].SubItems.Add(nickName);
                }
            }

            lv.Refresh();
        }

        private String GetDirectoryNickName(String dirName)
        {
            String result = "";
            //get the nickname from a list(Id,Nickname)
            int id = -1;
            try
            {
                id = int.Parse(dirName);
                result = GetNickById(id);
            }
            catch (Exception)
            {
                result = "no config dir";
            }

            return result;

        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private String GetNickById(int id)
        {
            foreach (var nickDataModel in _NickList)
            {
                if(nickDataModel.UserId == id)
                {
                    return nickDataModel.NickName;
                }
            }
            return "";
        }

        private void toolStripButtonToggleTestMode_Click(object sender, EventArgs e)
        {
            if (TestMode == 0)
            {
                TestMode = 1;
                toolStripButtonToggleTestMode.BackColor = Color.Red;
            }
            else
            {
                TestMode = 0;
                toolStripButtonToggleTestMode.BackColor = System.Drawing.SystemColors.Control; 
            }
            InitializeUi(TestMode);
        }

        private void checkBoxFilter1_CheckedChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void listViewLocalBackupFolder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = (ListView)(sender);
            DirectoryInfo dd = new DirectoryInfo(_MyConfigCopierData._LocalConfigPath);
            HandleListViewDoubleClick(lv, dd, "LocalBackup");
        }

        private void listViewServerBackupFolder_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = (ListView)(sender);
            DirectoryInfo dd = new DirectoryInfo(_MyConfigCopierData._ServerConfigPath);
            HandleListViewDoubleClick(lv, dd, "ServerBackup");
            #region commented switch on listview name
            //switch (lv.Name)
            //{
            //    case "listViewServerBackupFolder":
            //        {
            //            DirectoryInfo dd = new DirectoryInfo(_MyConfigCopierData._ServerConfigPath); 
            //            HandleListViewDoubleClick(lv,dd, "ServerBackup");
            //            break;
            //        }
            //    case "listViewLocalBackupFolder":
            //        {
            //            DirectoryInfo dd = new DirectoryInfo(_MyConfigCopierData._LocalConfigPath);
            //            HandleListViewDoubleClick(lv,dd,"LocalBackup");
            //            break;
            //        }
            //}
            #endregion commented switch on listview name
        }

        private void HandleListViewDoubleClick(ListView lv, DirectoryInfo currentDir, String lvMode)
        {
            string selected = "No Selection";
            if(lv.SelectedItems.Count > 0)
            {
                selected = lv.SelectedItems[0].Text;
                if (!string.IsNullOrEmpty(selected) && selected != "No Selection")
                {
                    String newDir;
                    if (selected == "..")
                    {
                        newDir = currentDir.Parent.FullName;
                    }
                    else
                    {
                        newDir = currentDir.FullName + "\\" + selected;
                    }

                    if (Directory.Exists(newDir))
                    {
                        if(lvMode == "ServerBackup")
                        {
                            _MyConfigCopierData._ServerConfigPath = newDir;
                            RefreshServerPathVars();
                        }
                        if (lvMode == "LocalBackup")
                        {
                            _MyConfigCopierData._LocalConfigPath = newDir;
                            RefreshLocalPathVars();
                        }

                        RefreshData();
                    }
                    else
                    {
                        MessageBox.Show("Not a valid directory (" + newDir + ")");
                    }
                }
            }
            else
            {
                MessageBox.Show(selected);
            }
        }

        private void RefreshServerPathVars()
        {
            textBoxServerBackupFolder.Text = _MyConfigCopierData._ServerConfigPath;
        }

        private void RefreshLocalPathVars()
        {
            textBoxLocalBackupFolder.Text = _MyConfigCopierData._LocalConfigPath;
        }

        private void buttonExitApp_Click(object sender, EventArgs e)
        {
            var dlgRes = MessageBox.Show(
                "Are you sure ?" + Environment.NewLine + "This will kill the entire application",
                "Confirm Exit",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);

            if (dlgRes == DialogResult.Yes)
            {
                Application.Exit();
            }            
        }


    }
}
