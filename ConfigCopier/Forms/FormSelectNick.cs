﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using ConfigCopier.Forms;
using ConfigCopier.Models;
using ConfigCopier.Services;
using MySql.Data.MySqlClient;
using System.Linq;
using SchumiLibrary;
using System.Net;

namespace ConfigCopier
{
    public partial class FormSelectNick : Form
    {
        //////////////////////////////////////////////////////////////////////////////////////
        // Info
        //////////////////////////////////////////////////////////////////////////////////////

        //5/3/2013
        // steam path
        // steam/steamapps/common/Half-Life\cstrike
        // ftp - sql server external ip : 83.101.57.101
        // ftp - sql server internal ip : 192.168.1.2
        
        //todo : 

        // login/backup/restore buttons with functionality
        // other nick button ?


        //////////////////////////////////////////////////////////////////////////////////////
        // Constants
        //////////////////////////////////////////////////////////////////////////////////////
        private const string modeBackup = "backup";
        private const string modeRestore = "restore";
        private const string defaultFtpPassword = "userftp";
        // tags
        private const string TAG_NAME = "%%name%%";
        private const string TAG_STEAM_NAME = "%%steamname%%";
        private const string TAG_LOL_NAME = "%%lolname%%";
        private const string TAG_PC_NAME = "%%pcname%%";
        private const string TAG_IP = "%%ip%%";


        //////////////////////////////////////////////////////////////////////////////////////
        // Members
        //////////////////////////////////////////////////////////////////////////////////////
        private bool _RunRemote = false;
        private bool _AdminEnabled = false;
        private bool _LoginNotifyEnabled = false;
        private bool _FormK_Pressed = false;

        public List<Models.NickDataModel> _NickList;
        public FormConfigCopier _FormConfigCopier;
        public FormAutomaticConfigCopier _FormAutomaticConfigCopier;

        // selected nick
        public Models.NickDataModel _SelectedNickName = null;
        //////////////////////////////////////////////////////////////////////////////////////

        public FormSelectNick()
        {
            // this sets everything done in the designer
            InitializeComponent();

            // personalised initiator
            Initialize();
        }

        private void Initialize()
        {

            // init on false
            SetBackupAvailability(false);
            
            _NickList = new List<NickDataModel>();
            _RunRemote = ConfigurationManager.AppSettings["RunParameterRemote"].ToUpper() == "TRUE"; 
            _LoginNotifyEnabled = ConfigurationManager.AppSettings["LoginNotifyEnabled"].ToUpper() == "TRUE";
            _AdminEnabled = ConfigurationManager.AppSettings["debugButton"].ToUpper() == "TRUE";
            if (_AdminEnabled) buttonAdmin.Visible = true;


            bool repeat = true; 
            while (repeat)
            {
                repeat = false; // set off ! 5/3/2013
                //var splash = new FormLoadingMask();
                // get interval from config
                //splash.Start(1000);
                //var connectionOk = splash.connTest;
                var connectionOk = ConfigCopierService.TestSqlServerConnection(labelSelect.Text);

                if(connectionOk)
                {
                    GetServerData();
                    repeat = false;
                }
                else
                {
                    // set form title info
                    //repeat = false;

                    //no messages ??
                    //var dlgResult = MessageBox.Show(
                    //    "Unable to contact the ZoomRoom servers" + Environment.NewLine +
                    //    "(" + ConfigurationSettings.AppSettings["SERVERCONFIGPATH"].ToString() + ")" + Environment.NewLine +
                    //    "Try again ?",
                    //    "ConfigCopier : Servers off line",
                    //    MessageBoxButtons.YesNo,
                    //    MessageBoxIcon.Error);
                    //if (dlgResult == DialogResult.No)
                    //{
                    //    repeat = false;
                    //}
                }
            }
        }

        private void GetServerData()
        {

            var connectionString = ConfigurationManager.AppSettings["MasterReader.ConnectionString"];
            var connectionStringRemote = ConfigurationManager.AppSettings["MasterReaderRemote.ConnectionString"];
            var server = "";
            var db = "";

            // <add
            // key="MasterReader.ConnectionString"
            // value="Server=mysqlhost.jouwdomein.be;Database=D0501003;Uid=U0501003;Pwd=;"
            // />
            // <!--root : tbl_ZoomRoomUsers-->

            //MySqlServer
            MySqlConnection myConnection = new MySqlConnection();
            myConnection.ConnectionString = _RunRemote ? connectionStringRemote : connectionString;

            // Result container
            //string tmpQuery = "SELECT id,NickName from tbl_ZoomRoomUsers";
            string tmpQuery = "SELECT id,name,killer,aidPoints,steamname,ip from users ";

            bool onlyKnownSteamUsers = ConfigurationManager.AppSettings["bOnlyKnownSteamUsers"].ToUpper() == "TRUE";

            if (onlyKnownSteamUsers)
            {
                tmpQuery += " where steamname != '' ";
            }

            String orderBy = " order by name ASC ";

            tmpQuery += orderBy;

            try
            {
                myConnection.Open();

                server = myConnection.DataSource;
                db = myConnection.Database;
 
                MySqlCommand myCommand = new MySqlCommand(tmpQuery, myConnection);
                MySqlDataReader myReader = null;
                myReader = myCommand.ExecuteReader();
                var i = 1;

                _NickList.Clear();

                while (myReader.Read())
                {
                    var nickDataModel = new Models.NickDataModel(
                        myReader["name"].ToString(),
                        myReader["steamname"].ToString(),
                        "", "", 
                        (int)myReader["id"],
                        Convert.ToDecimal(myReader["killer"]),
                        Convert.ToDecimal(myReader["aidPoints"]));
                    _NickList.Add(nickDataModel);
                    i++;
                }

                myConnection.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                if(myConnection != null)
                {
                    myConnection.Close();
                }
            }

            //todo : use linq, done in sql now
            //SortNickList();

            // set + update grid
            InitGrid();

            //HC : no server info !!
            //textBoxSource.Text = "Source : [DATA FROM " + server + "/" + db + "]";
            textBoxSource.Text = "Source : [DATA FROM SQL SERVER]";

            if (_RunRemote) { textBoxSource.Text += " **remote**"; }
        }

        private void InitGrid()
        {
            if (_NickList != null)
            {
                textBoxSearchNickName.Focus();

                dataGridViewNickData.DataSource = null;

                dataGridViewNickData.DataSource = _NickList;

                dataGridViewNickData.Columns[0].Width = 160;
                dataGridViewNickData.Columns[1].Width = 120;
                //dataGridViewNickData.Columns[2].Width = 160;
                //dataGridViewNickData.Columns[3].Width = 40;

                dataGridViewNickData.ClearSelection();

                dataGridViewNickData.Refresh();

                // set focus to search edit
                textBoxSearchNickName.Select();
                //textBoxSearchNickName.Focus();

            }
            else
            {
                MessageBox.Show("NickList null !");
            }
        }

        private bool CheckFileServerSilent()
        {
            string serverPath = ConfigurationSettings.AppSettings["SERVERCONFIGPATH"].ToString();
            bool serverOk = ConfigCopierService.CheckDirectoryPath(Path.GetPathRoot(serverPath));
            if(serverOk)
            {
                this.Text += " [" + serverPath + "]";
            }
            return serverOk;
        }

        private bool CheckFileServerWithMessage()
        {
            if (CheckFileServerSilent())
            {
                return true;
            }
            else
            {
                MessageBox.Show(
                    "Unable to contact the ZoomRoom file server" + Environment.NewLine +
                    "(" + ConfigurationSettings.AppSettings["SERVERCONFIGPATH"].ToString() + ")" + Environment.NewLine +
                    "Check your connectivity and try again in a minute or 2",
                    "ConfigCopier : Server off line",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }

        private void NotifyServerOfLogin(int userId)
        {
            var timerFrequency = ConfigurationManager.AppSettings["LoginNotifyUrlFrequency"];
            var url = _RunRemote ? ConfigurationManager.AppSettings["LoginNotifyUrlRemote"] : ConfigurationManager.AppSettings["LoginNotifyUrl"];
            url += "?usrId=" + userId;
            string ip = SchumiLibrary.Services.SystemService.GetLocalIP();
            url += "&ip=" + ip;

            try
            {

                //library and HttpWebRequest HttpWebResponse classes
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //set proxy
                //var cr = new NetworkCredential("automation", "dkma", "MACADAM");
                //if (useProxy)
                //{
                //    request.Proxy = new WebProxy("proxy.macadam.local", 8080);
                //    request.Proxy.Credentials = cr;
                //}
                //else
                //{
                    request.Proxy = null;
                //}

                var resp = request.GetResponse();
                //Stream stream = resp.GetResponseStream();
                //StreamReader reader = new StreamReader(stream);
                //MessageBox.Show(reader.ReadToEnd());

                //Response.Write(reader.ReadToEnd);
            }
            catch (Exception e)
            {
                //Add error logging here !            
            }

        }

        private void Proceed(string mode)
        {
            if (dataGridViewNickData.SelectedRows != null && dataGridViewNickData.SelectedRows.Count > 0)
            {
                _SelectedNickName = (Models.NickDataModel)dataGridViewNickData.SelectedRows[0].DataBoundItem; 

                // on selected get values from this form var
                if (_SelectedNickName != null)
                {
                    //Notify server of login if enabled
                    if (_LoginNotifyEnabled && mode == modeRestore)
                    {
                        // Get/set ip
                        _SelectedNickName.SetIp(SchumiLibrary.Services.SystemService.GetLocalIP());
                        //Get/set pcName
                        _SelectedNickName.SetPcName(SchumiLibrary.Services.SystemService.GetLocalPcName());

                        NotifyServerOfLogin(_SelectedNickName.UserId);
                    } 

                    // enable backup after login
                    SetBackupAvailability(true);

                    if (mode == "restoreinpanel")
                    {
                        // create instance of the automatic copier form
                        var automaticConfigCopierForm = new FormAutomaticConfigCopier(null, _NickList, modeRestore);
                        //this.WindowState = FormWindowState.Minimized;
                        //automaticConfigCopierForm.ShowDialog();

                        automaticConfigCopierForm.TopLevel = false;
                        this.panelCopyInfo.Controls.Add(automaticConfigCopierForm);
                        automaticConfigCopierForm.Show();
                        automaticConfigCopierForm.Dock = DockStyle.Top;
                        automaticConfigCopierForm.BringToFront();
                    }
                    else
                    {
                        if (mode == modeRestore)
                        {
                            try
                            {
                                // create instance of the automatic copier form
                                var automaticConfigCopierForm = new FormAutomaticConfigCopier(_SelectedNickName, _NickList, mode);
                                //this.WindowState = FormWindowState.Minimized;
                                //automaticConfigCopierForm.ShowDialog();

                                automaticConfigCopierForm.Close();

                                string msg = " has been logged in ! \nYour settings for CS 1.6 are set !!";
                                msg = msg + "\n\nUse the bullet icon to come back to this tool";

                                // message on succeed
                                MessageBox.Show(_SelectedNickName.NickName + msg);

                                this.WindowState = FormWindowState.Minimized;
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("something went wrong with your login/restore ...!!");
                            }
                        }
                        else //backup                        
                        {
                            try
                            {
                                // create instance of the automatic copier form
                                var automaticConfigCopierForm = new FormAutomaticConfigCopier(_SelectedNickName, _NickList, mode);
                                //this.WindowState = FormWindowState.Minimized;
                                //automaticConfigCopierForm.ShowDialog();

                                automaticConfigCopierForm.Close();

                                string msg = " config was backed up on the server!" ;
                                msg = msg + "\n\nUse the bullet icon to come back to this tool";

                                // message on succeed
                                MessageBox.Show(_SelectedNickName.NickName + msg);

                                this.WindowState = FormWindowState.Minimized;
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("something went wrong with your backup ...!!");
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No valid nickname selected !", "Select nickname");
                }
            }
            else
            {
                MessageBox.Show("No selection ?", "Select nickname");
            }
        }

        private void StartUpAppsFromConfig()
        {
            List<String> startUpItems = new List<string>();
            startUpItems = SchumiLibrary.Services.ConfigService.GetConfigValuesWithKeyLike("StartUp");

            string errInfo = "";
            foreach (String startUpItem in startUpItems)
            {
                string valFromConfig = startUpItem;

                try
                {
                    // replace tags with data with niclmodel data 
                    valFromConfig = valFromConfig.Replace(TAG_NAME, _SelectedNickName.NickName);
                    valFromConfig = valFromConfig.Replace(TAG_STEAM_NAME, _SelectedNickName.SteamName);
                    valFromConfig = valFromConfig.Replace(TAG_LOL_NAME, _SelectedNickName.GameIdLol);
                    valFromConfig = valFromConfig.Replace(TAG_PC_NAME, _SelectedNickName.GetPcName());
                    valFromConfig = valFromConfig.Replace(TAG_IP, _SelectedNickName.GetIp());

                    // split for app - parameters
                    var sep = new string[] { "$$" };
                    var arr = valFromConfig.Split(sep, StringSplitOptions.None);
                    //app
                    var arg0 = arr[0];
                    errInfo = arg0;
                    // parameters
                    var arg1 = "";
                    if (arr.Count() > 1)
                    {
                        arg1 = arr[1];
                        errInfo += " " + arg1;
                    }

                    // startup command
                    SchumiLibrary.Services.SystemService.ExecuteCommand(arg0, arg1);
                }
                catch (Exception E)
                {
                    string localMessage = "[" + startUpItem + "][" + valFromConfig + "] " + errInfo + " " + E.Message;
                    MessageBox.Show(localMessage);
                }

            }


        }

        private void SetBackupAvailability(bool val) 
        {
            buttonBackUp.Enabled = val;
        }

        private void dataGridViewNickData_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Proceed("restore"); // removed !!
            var selectedKnickNameString = "";
            var selectedNickNameObj = (Models.NickDataModel)dataGridViewNickData.SelectedRows[0].DataBoundItem;

            // on selected get values from this form var
            if (selectedNickNameObj != null)
            {
                selectedKnickNameString = selectedNickNameObj.NickName;
            }
            MessageBox.Show("Use Login button to restore the config for the selected NickName [" + selectedKnickNameString + "]");
        }

        private void dataGridViewNickData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // ToDo : sort grid on HeaderClick

            //  0 : name
            //  1 : killer
            //  2: pcid
            //  MessageBox.Show(e.ColumnIndex.ToString());
            //  e.ColumnIndex;
        }

        private void FormSelectNick_Shown(object sender, EventArgs e)
        {
            dataGridViewNickData.ClearSelection();
        }


        private void buttonGetData_Click(object sender, EventArgs e)
        {
            GetServerData();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            Proceed("restore");

            StartUpAppsFromConfig();
        }

        private void buttonBackUp_Click(object sender, EventArgs e)
        {
            Proceed(modeBackup);
        }

        private void labelSelect_DoubleClick(object sender, EventArgs e)
        {
            //enter password ?

            // create instance of the automatic copier form
            var automaticConfigCopierForm = new FormAutomaticConfigCopier(null, _NickList, "no automatic startup");
            //this.WindowState = FormWindowState.Minimized;
            automaticConfigCopierForm.ShowDialog();
        }


        // Admin  button
        private void buttonAdmin_Click(object sender, EventArgs e)
        {
            StartUpAppsFromConfig();

            //Proceed("restoreinpanel");

            //TestFtp2();

            //var dlgResult = MessageBox.Show(
            //    "Start explorer.exe ?", "Admin",
            //    MessageBoxButtons.YesNo,
            //    MessageBoxIcon.Question);
            //if (dlgResult == DialogResult.Yes)
            //{
            //    SchumiLibrary.Services.SystemService.ExecuteCommand("explorer.exe", "");
            //}

        }

        //AutoSearch
        private void textBoxSearchNickName_KeyUp(object sender, KeyEventArgs e)
        {
            var dGridView = dataGridViewNickData;
            String searchText = ((TextBox)sender).Text.ToLower();
            int searchMinLen = 0;
            int columnId = 0; // name column ?

            int searchTextLen = searchText.Length;
            if (searchTextLen > searchMinLen)
            {
                bool itemFound = false;
                int idx = 0;
                int nmbrOfRows = dGridView.RowCount;
                while (!itemFound && idx < nmbrOfRows)
                {
                    var item = dGridView[columnId, idx]; // search in column columnId
                    String celVal = item.Value.ToString().ToLower();
                    if (celVal.Length >= searchTextLen)
                    {
                        String searchVal = celVal.Substring(0, searchTextLen);
                        if (searchVal == searchText) // value starts with searchstring
                        {
                            if (dGridView.CurrentCell == dGridView[columnId, idx])
                                dGridView.CurrentCell = dGridView[columnId, 0]; //force movement if scrolled without selected change
                            dGridView.CurrentCell = dGridView[columnId, idx];

                            itemFound = true;
                        }
                    }

                    idx++;
                }
            }

        }

        // detect ESC and K
        private void FormSelectNick_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.K && !_FormK_Pressed)
            {
                _FormK_Pressed = true;
            }

            if (e.KeyCode == Keys.Escape) { this.Close(); }
        }

        private void FormSelectNick_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.K && _FormK_Pressed)
            {
                _FormK_Pressed = false;
            }

        }

        // Hidden action
        private void textBoxSource_MouseClick(object sender, MouseEventArgs e)
        {
            // if keyboard == K : 
            if (_FormK_Pressed)
            {
                if (e.Button == MouseButtons.Left) 
                {
                    try 
                    {
                        ConfigCopierService.MountEncryptedPartition();
                    }
                    catch (Exception ex)
                    {
                        _FormK_Pressed = false;
                        MessageBox.Show(ex.Message);             
                    }
                }
            }

        }



        // Test functions
        private void TestFtp()
        {
            try
            {
                var n = new NickDataModel("PainKiller", "gaming_schumipage_be", "Jo", "Schoenmakers", 64, 101, 22);
                if (1 != null)
                //if (_FormAutomaticConfigCopier != null)
                {
                    //_FormAutomaticConfigCopier.getFileViaFtp(n);

                    var server = ConfigurationManager.AppSettings["ftpServer"];
                    var serverRemote = ConfigurationManager.AppSettings["ftpServerRemote"];
                    var usr = ConfigurationManager.AppSettings["ftpUser"];
                    var pss = ConfigurationManager.AppSettings["ftpPassword"];
                    if (String.IsNullOrEmpty(pss)) pss = defaultFtpPassword;
                    var ftpPath = ConfigurationManager.AppSettings["ftpPath"];
                    var ftpClient = new ConfigCopier.Services.FtpService(server, usr, pss);
                    //var dirList = ftpClient.DirectoryDetails(ftpPath);
                    var origin = ftpPath + "/" + n.UserId + "/" + "config.cfg";
                    var destination = @"C:\tempp\config\config.cfg";
                    var errLog = new List<String>();
                    errLog = ftpClient.DownloadFile(origin, destination);
                    if (errLog.Count > 0) MessageBox.Show(errLog.First());
                    //MessageBox.Show(dirList.First().PathOrFilename);
                }
                else
                {
                    MessageBox.Show("F*ck !!!");
                }

            }
            catch (Exception)
            {
                MessageBox.Show("button 1 error");

                throw;
            }
        }

        private void TestFtp2()
        {
            //knalkot
            //var ftpServer = ConfigurationManager.AppSettings["ftpServer"];
            //var ftpServerRemote = ConfigurationManager.AppSettings["ftpServerRemote"];
            //var ftpUser = ConfigurationManager.AppSettings["ftpUser"];
            //var ftpPassword = "userftp";
            //var userId = 64;
            //ss
            var ftpServer = "ftp://ftp.schumipage.be/"; // has to begin with ftp and end on /
            var ftpUser = "U0501003";
            var ftpPassword = "fill this for testing";
            var ftpDirectory = "64"; // without initial / and no / at the end

            String cfgDir = ConfigurationManager.AppSettings["HalfLifeConfigPath"];

            // list all *.cfg files
            List<FileInfo> listCfgFiles = SchumiLibrary.Services.SystemService.GetAllFilesInDir(cfgDir, "cfg");
            // delete all files in list
            foreach (FileInfo fileInfo in listCfgFiles)
            {
                string path = fileInfo.FullName;
                File.Delete(path);
            }

            ConfigCopierService.getDirFilesViaFtp(ftpServer, ftpUser, ftpPassword, ftpDirectory, cfgDir, ".cfg");
        }

    }
    

}
