﻿namespace ConfigCopier.Forms
{
    partial class FormConfigInspector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfigInspector));
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripLabelNickName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonToggleTestMode = new System.Windows.Forms.ToolStripButton();
            this.labelLocalHalflifeFolder = new System.Windows.Forms.Label();
            this.listViewLocalHalfLifeFolder = new System.Windows.Forms.ListView();
            this.FileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.configNickName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxLocalHalfLifeFolder = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelServerBackupFolder = new System.Windows.Forms.Label();
            this.textBoxServerBackupFolder = new System.Windows.Forms.TextBox();
            this.listViewServerBackupFolder = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelLocalBackupFolder = new System.Windows.Forms.Label();
            this.textBoxLocalBackupFolder = new System.Windows.Forms.TextBox();
            this.listViewLocalBackupFolder = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkBoxFilter1 = new System.Windows.Forms.CheckBox();
            this.buttonExitApp = new System.Windows.Forms.Button();
            this.toolStripMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMain
            // 
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelNickName,
            this.toolStripSeparator1,
            this.toolStripButtonRefresh,
            this.toolStripSeparator2,
            this.toolStripButtonToggleTestMode});
            this.toolStripMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.Size = new System.Drawing.Size(872, 28);
            this.toolStripMain.TabIndex = 2;
            this.toolStripMain.Text = "toolStripMain";
            // 
            // toolStripLabelNickName
            // 
            this.toolStripLabelNickName.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabelNickName.Name = "toolStripLabelNickName";
            this.toolStripLabelNickName.Size = new System.Drawing.Size(104, 25);
            this.toolStripLabelNickName.Text = "NickName";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripButtonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRefresh.Image")));
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(71, 25);
            this.toolStripButtonRefresh.Text = "Refresh";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripButtonToggleTestMode
            // 
            this.toolStripButtonToggleTestMode.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonToggleTestMode.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripButtonToggleTestMode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonToggleTestMode.Image")));
            this.toolStripButtonToggleTestMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonToggleTestMode.Name = "toolStripButtonToggleTestMode";
            this.toolStripButtonToggleTestMode.Size = new System.Drawing.Size(120, 25);
            this.toolStripButtonToggleTestMode.Text = "Toggle TestMode";
            this.toolStripButtonToggleTestMode.Click += new System.EventHandler(this.toolStripButtonToggleTestMode_Click);
            // 
            // labelLocalHalflifeFolder
            // 
            this.labelLocalHalflifeFolder.AutoSize = true;
            this.labelLocalHalflifeFolder.Location = new System.Drawing.Point(13, 10);
            this.labelLocalHalflifeFolder.Name = "labelLocalHalflifeFolder";
            this.labelLocalHalflifeFolder.Size = new System.Drawing.Size(35, 13);
            this.labelLocalHalflifeFolder.TabIndex = 7;
            this.labelLocalHalflifeFolder.Text = "label1";
            // 
            // listViewLocalHalfLifeFolder
            // 
            this.listViewLocalHalfLifeFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewLocalHalfLifeFolder.AutoArrange = false;
            this.listViewLocalHalfLifeFolder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.FileName,
            this.configNickName});
            this.listViewLocalHalfLifeFolder.GridLines = true;
            this.listViewLocalHalfLifeFolder.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewLocalHalfLifeFolder.HideSelection = false;
            this.listViewLocalHalfLifeFolder.Location = new System.Drawing.Point(16, 52);
            this.listViewLocalHalfLifeFolder.MultiSelect = false;
            this.listViewLocalHalfLifeFolder.Name = "listViewLocalHalfLifeFolder";
            this.listViewLocalHalfLifeFolder.ShowGroups = false;
            this.listViewLocalHalfLifeFolder.ShowItemToolTips = true;
            this.listViewLocalHalfLifeFolder.Size = new System.Drawing.Size(389, 133);
            this.listViewLocalHalfLifeFolder.TabIndex = 6;
            this.listViewLocalHalfLifeFolder.UseCompatibleStateImageBehavior = false;
            this.listViewLocalHalfLifeFolder.View = System.Windows.Forms.View.Details;
            // 
            // FileName
            // 
            this.FileName.Text = "File name";
            this.FileName.Width = 270;
            // 
            // configNickName
            // 
            this.configNickName.Text = "NickName";
            this.configNickName.Width = 100;
            // 
            // textBoxLocalHalfLifeFolder
            // 
            this.textBoxLocalHalfLifeFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLocalHalfLifeFolder.Location = new System.Drawing.Point(16, 26);
            this.textBoxLocalHalfLifeFolder.Name = "textBoxLocalHalfLifeFolder";
            this.textBoxLocalHalfLifeFolder.ReadOnly = true;
            this.textBoxLocalHalfLifeFolder.Size = new System.Drawing.Size(389, 20);
            this.textBoxLocalHalfLifeFolder.TabIndex = 5;
            this.textBoxLocalHalfLifeFolder.Text = "Local HalfLife Folder";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.labelLocalHalflifeFolder);
            this.panel1.Controls.Add(this.textBoxLocalHalfLifeFolder);
            this.panel1.Controls.Add(this.listViewLocalHalfLifeFolder);
            this.panel1.Location = new System.Drawing.Point(12, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 200);
            this.panel1.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.SystemColors.Info;
            this.panel3.Controls.Add(this.labelServerBackupFolder);
            this.panel3.Controls.Add(this.textBoxServerBackupFolder);
            this.panel3.Controls.Add(this.listViewServerBackupFolder);
            this.panel3.Location = new System.Drawing.Point(440, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(422, 406);
            this.panel3.TabIndex = 17;
            // 
            // labelServerBackupFolder
            // 
            this.labelServerBackupFolder.AutoSize = true;
            this.labelServerBackupFolder.Location = new System.Drawing.Point(13, 10);
            this.labelServerBackupFolder.Name = "labelServerBackupFolder";
            this.labelServerBackupFolder.Size = new System.Drawing.Size(35, 13);
            this.labelServerBackupFolder.TabIndex = 7;
            this.labelServerBackupFolder.Text = "label1";
            // 
            // textBoxServerBackupFolder
            // 
            this.textBoxServerBackupFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxServerBackupFolder.Location = new System.Drawing.Point(16, 26);
            this.textBoxServerBackupFolder.Name = "textBoxServerBackupFolder";
            this.textBoxServerBackupFolder.ReadOnly = true;
            this.textBoxServerBackupFolder.Size = new System.Drawing.Size(389, 20);
            this.textBoxServerBackupFolder.TabIndex = 5;
            this.textBoxServerBackupFolder.Text = "Server Backup Folder";
            // 
            // listViewServerBackupFolder
            // 
            this.listViewServerBackupFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewServerBackupFolder.AutoArrange = false;
            this.listViewServerBackupFolder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewServerBackupFolder.FullRowSelect = true;
            this.listViewServerBackupFolder.GridLines = true;
            this.listViewServerBackupFolder.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewServerBackupFolder.HideSelection = false;
            this.listViewServerBackupFolder.Location = new System.Drawing.Point(16, 52);
            this.listViewServerBackupFolder.MultiSelect = false;
            this.listViewServerBackupFolder.Name = "listViewServerBackupFolder";
            this.listViewServerBackupFolder.ShowGroups = false;
            this.listViewServerBackupFolder.ShowItemToolTips = true;
            this.listViewServerBackupFolder.Size = new System.Drawing.Size(389, 339);
            this.listViewServerBackupFolder.TabIndex = 6;
            this.listViewServerBackupFolder.UseCompatibleStateImageBehavior = false;
            this.listViewServerBackupFolder.View = System.Windows.Forms.View.Details;
            this.listViewServerBackupFolder.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewServerBackupFolder_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "File name";
            this.columnHeader1.Width = 270;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "NickName";
            this.columnHeader2.Width = 100;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.labelLocalBackupFolder);
            this.panel2.Controls.Add(this.textBoxLocalBackupFolder);
            this.panel2.Controls.Add(this.listViewLocalBackupFolder);
            this.panel2.Location = new System.Drawing.Point(12, 264);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(422, 200);
            this.panel2.TabIndex = 18;
            // 
            // labelLocalBackupFolder
            // 
            this.labelLocalBackupFolder.AutoSize = true;
            this.labelLocalBackupFolder.Location = new System.Drawing.Point(13, 10);
            this.labelLocalBackupFolder.Name = "labelLocalBackupFolder";
            this.labelLocalBackupFolder.Size = new System.Drawing.Size(35, 13);
            this.labelLocalBackupFolder.TabIndex = 7;
            this.labelLocalBackupFolder.Text = "label1";
            // 
            // textBoxLocalBackupFolder
            // 
            this.textBoxLocalBackupFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLocalBackupFolder.Location = new System.Drawing.Point(16, 26);
            this.textBoxLocalBackupFolder.Name = "textBoxLocalBackupFolder";
            this.textBoxLocalBackupFolder.ReadOnly = true;
            this.textBoxLocalBackupFolder.Size = new System.Drawing.Size(389, 20);
            this.textBoxLocalBackupFolder.TabIndex = 5;
            this.textBoxLocalBackupFolder.Text = "Local Backup Folder";
            // 
            // listViewLocalBackupFolder
            // 
            this.listViewLocalBackupFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewLocalBackupFolder.AutoArrange = false;
            this.listViewLocalBackupFolder.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.listViewLocalBackupFolder.FullRowSelect = true;
            this.listViewLocalBackupFolder.GridLines = true;
            this.listViewLocalBackupFolder.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewLocalBackupFolder.HideSelection = false;
            this.listViewLocalBackupFolder.Location = new System.Drawing.Point(16, 52);
            this.listViewLocalBackupFolder.MultiSelect = false;
            this.listViewLocalBackupFolder.Name = "listViewLocalBackupFolder";
            this.listViewLocalBackupFolder.ShowGroups = false;
            this.listViewLocalBackupFolder.ShowItemToolTips = true;
            this.listViewLocalBackupFolder.Size = new System.Drawing.Size(389, 133);
            this.listViewLocalBackupFolder.TabIndex = 6;
            this.listViewLocalBackupFolder.UseCompatibleStateImageBehavior = false;
            this.listViewLocalBackupFolder.View = System.Windows.Forms.View.Details;
            this.listViewLocalBackupFolder.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewLocalBackupFolder_MouseDoubleClick);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "File name";
            this.columnHeader3.Width = 270;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NickName";
            this.columnHeader4.Width = 100;
            // 
            // checkBoxFilter1
            // 
            this.checkBoxFilter1.AutoSize = true;
            this.checkBoxFilter1.Checked = true;
            this.checkBoxFilter1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFilter1.Location = new System.Drawing.Point(12, 35);
            this.checkBoxFilter1.Name = "checkBoxFilter1";
            this.checkBoxFilter1.Size = new System.Drawing.Size(88, 17);
            this.checkBoxFilter1.TabIndex = 8;
            this.checkBoxFilter1.Text = "Filter on *.cfg";
            this.checkBoxFilter1.UseVisualStyleBackColor = true;
            this.checkBoxFilter1.CheckedChanged += new System.EventHandler(this.checkBoxFilter1_CheckedChanged);
            // 
            // buttonExitApp
            // 
            this.buttonExitApp.Location = new System.Drawing.Point(819, 31);
            this.buttonExitApp.Name = "buttonExitApp";
            this.buttonExitApp.Size = new System.Drawing.Size(43, 23);
            this.buttonExitApp.TabIndex = 19;
            this.buttonExitApp.Text = "X";
            this.buttonExitApp.UseVisualStyleBackColor = true;
            this.buttonExitApp.Click += new System.EventHandler(this.buttonExitApp_Click);
            // 
            // FormConfigInspector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(872, 475);
            this.Controls.Add(this.buttonExitApp);
            this.Controls.Add(this.checkBoxFilter1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStripMain);
            this.Name = "FormConfigInspector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Config inspector";
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStripLabel toolStripLabelNickName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label labelLocalHalflifeFolder;
        private System.Windows.Forms.ListView listViewLocalHalfLifeFolder;
        private System.Windows.Forms.TextBox textBoxLocalHalfLifeFolder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader FileName;
        private System.Windows.Forms.ColumnHeader configNickName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labelServerBackupFolder;
        private System.Windows.Forms.TextBox textBoxServerBackupFolder;
        private System.Windows.Forms.ListView listViewServerBackupFolder;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelLocalBackupFolder;
        private System.Windows.Forms.TextBox textBoxLocalBackupFolder;
        private System.Windows.Forms.ListView listViewLocalBackupFolder;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ToolStripButton toolStripButtonToggleTestMode;
        private System.Windows.Forms.CheckBox checkBoxFilter1;
        private System.Windows.Forms.Button buttonExitApp;

    }
}