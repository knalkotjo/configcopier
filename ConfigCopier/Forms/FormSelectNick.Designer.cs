﻿namespace ConfigCopier
{
    partial class FormSelectNick
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectNick));
            this.dataGridViewNickData = new System.Windows.Forms.DataGridView();
            this.buttonGetData = new System.Windows.Forms.Button();
            this.textBoxSource = new System.Windows.Forms.TextBox();
            this.labelSelect = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonBackUp = new System.Windows.Forms.Button();
            this.buttonAdmin = new System.Windows.Forms.Button();
            this.panelCopyInfo = new System.Windows.Forms.Panel();
            this.textBoxSearchNickName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNickData)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewNickData
            // 
            this.dataGridViewNickData.AllowUserToAddRows = false;
            this.dataGridViewNickData.AllowUserToDeleteRows = false;
            this.dataGridViewNickData.AllowUserToOrderColumns = true;
            this.dataGridViewNickData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Info;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewNickData.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewNickData.GridColor = System.Drawing.SystemColors.Info;
            this.dataGridViewNickData.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridViewNickData.Location = new System.Drawing.Point(31, 63);
            this.dataGridViewNickData.MultiSelect = false;
            this.dataGridViewNickData.Name = "dataGridViewNickData";
            this.dataGridViewNickData.ReadOnly = true;
            this.dataGridViewNickData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewNickData.ShowEditingIcon = false;
            this.dataGridViewNickData.Size = new System.Drawing.Size(672, 608);
            this.dataGridViewNickData.TabIndex = 0;
            this.dataGridViewNickData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewNickData_CellDoubleClick);
            this.dataGridViewNickData.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewNickData_ColumnHeaderMouseClick);
            // 
            // buttonGetData
            // 
            this.buttonGetData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGetData.Location = new System.Drawing.Point(309, 683);
            this.buttonGetData.Name = "buttonGetData";
            this.buttonGetData.Size = new System.Drawing.Size(206, 26);
            this.buttonGetData.TabIndex = 1;
            this.buttonGetData.Text = "Refresh if no Data";
            this.buttonGetData.UseVisualStyleBackColor = true;
            this.buttonGetData.Click += new System.EventHandler(this.buttonGetData_Click);
            // 
            // textBoxSource
            // 
            this.textBoxSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxSource.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBoxSource.Location = new System.Drawing.Point(31, 687);
            this.textBoxSource.Name = "textBoxSource";
            this.textBoxSource.ReadOnly = true;
            this.textBoxSource.Size = new System.Drawing.Size(272, 20);
            this.textBoxSource.TabIndex = 2;
            this.textBoxSource.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBoxSource_MouseClick);
            // 
            // labelSelect
            // 
            this.labelSelect.AutoSize = true;
            this.labelSelect.BackColor = System.Drawing.Color.Transparent;
            this.labelSelect.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelect.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelSelect.Location = new System.Drawing.Point(35, 16);
            this.labelSelect.Name = "labelSelect";
            this.labelSelect.Size = new System.Drawing.Size(309, 33);
            this.labelSelect.TabIndex = 3;
            this.labelSelect.Text = "Select your NickName";
            this.labelSelect.DoubleClick += new System.EventHandler(this.labelSelect_DoubleClick);
            // 
            // buttonOk
            // 
            this.buttonOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Location = new System.Drawing.Point(584, 21);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(119, 33);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "Login";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonBackUp
            // 
            this.buttonBackUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBackUp.Location = new System.Drawing.Point(602, 683);
            this.buttonBackUp.Name = "buttonBackUp";
            this.buttonBackUp.Size = new System.Drawing.Size(101, 26);
            this.buttonBackUp.TabIndex = 5;
            this.buttonBackUp.Text = "Backup";
            this.buttonBackUp.UseVisualStyleBackColor = true;
            this.buttonBackUp.Click += new System.EventHandler(this.buttonBackUp_Click);
            // 
            // buttonAdmin
            // 
            this.buttonAdmin.Location = new System.Drawing.Point(521, 685);
            this.buttonAdmin.Name = "buttonAdmin";
            this.buttonAdmin.Size = new System.Drawing.Size(75, 23);
            this.buttonAdmin.TabIndex = 6;
            this.buttonAdmin.Text = "Admin";
            this.buttonAdmin.UseVisualStyleBackColor = true;
            this.buttonAdmin.Visible = false;
            this.buttonAdmin.Click += new System.EventHandler(this.buttonAdmin_Click);
            // 
            // panelCopyInfo
            // 
            this.panelCopyInfo.Location = new System.Drawing.Point(734, 63);
            this.panelCopyInfo.Name = "panelCopyInfo";
            this.panelCopyInfo.Size = new System.Drawing.Size(414, 608);
            this.panelCopyInfo.TabIndex = 7;
            this.panelCopyInfo.Visible = false;
            // 
            // textBoxSearchNickName
            // 
            this.textBoxSearchNickName.Location = new System.Drawing.Point(404, 29);
            this.textBoxSearchNickName.Name = "textBoxSearchNickName";
            this.textBoxSearchNickName.Size = new System.Drawing.Size(174, 20);
            this.textBoxSearchNickName.TabIndex = 8;
            this.textBoxSearchNickName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxSearchNickName_KeyUp);
            // 
            // FormSelectNick
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1179, 714);
            this.Controls.Add(this.textBoxSearchNickName);
            this.Controls.Add(this.panelCopyInfo);
            this.Controls.Add(this.buttonAdmin);
            this.Controls.Add(this.buttonBackUp);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelSelect);
            this.Controls.Add(this.textBoxSource);
            this.Controls.Add(this.buttonGetData);
            this.Controls.Add(this.dataGridViewNickData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(400, 250);
            this.Name = "FormSelectNick";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select NickName";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormSelectNick_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormSelectNick_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormSelectNick_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNickData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewNickData;
        private System.Windows.Forms.Button buttonGetData;
        private System.Windows.Forms.TextBox textBoxSource;
        private System.Windows.Forms.Label labelSelect;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonBackUp;
        private System.Windows.Forms.Button buttonAdmin;
        private System.Windows.Forms.Panel panelCopyInfo;
        private System.Windows.Forms.TextBox textBoxSearchNickName;
    }
}