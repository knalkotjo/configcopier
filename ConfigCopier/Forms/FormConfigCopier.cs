﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConfigCopier
{
    public partial class FormConfigCopier : Form
    {

        private Models.NickDataModel _NickName;

        private FormConfigCopier()
        {
        }

        public FormConfigCopier(Models.NickDataModel nickName)
        {
            _NickName = nickName;

            InitializeComponent();
        }

        private void buttonOpenSourceFileDialog_Click(object sender, EventArgs e)
        {
            using (openFileDialogSourceFile)
            {
                //openFileDialogSourceFile.InitialDirectory = "c:\\";
                //openFileDialogSourceFile.Filter = "Excel files (*.xls)|*.xls";

                //openFileDialogSourceFile.FileName = Properties.Settings.Default["SourceFile"].ToString(); 

                // Select folder
                //FolderBrowserDialog fb = new FolderBrowserDialog();
                //System.Windows.Forms.DialogResult dr = fb.ShowDialog();
                //if (dr == DialogResult.OK)
                //    MessageBox.Show(fb.SelectedPath);


                switch (openFileDialogSourceFile.ShowDialog())
                {
                    case DialogResult.OK:
                        try
                        {
                            textBoxSourceFile.Text = openFileDialogSourceFile.FileName;
                            //Properties.Settings.Default["SourceFile"] = openFileDialogSourceFile.FileName;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Problem occured on selecting file: " + ex.Message);
                        }
                        break;
                }
            }

        }

        private void buttonOpenDestinationDir_Click(object sender, EventArgs e)
        {
            using (openFileDialogDestinationDir)
            {
                //openFileDialogSourceFile.InitialDirectory = "c:\\";
                //openFileDialogSourceFile.Filter = "Excel files (*.xls)|*.xls";

                switch (openFileDialogDestinationDir.ShowDialog())
                {
                    case DialogResult.OK:
                        try
                        {
                            textBoxDestinationDirectory.Text = openFileDialogDestinationDir.FileName;
                            //Properties.Settings.Default["DestinationDir"] = openFileDialogDestinationDir.FileName;

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Problem occured on selecting file: " + ex.Message);
                        }
                        break;
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            MessageBox.Show("R U Sure ?");
        }
    }
}
