﻿namespace ConfigCopier
{
    partial class FormAutomaticConfigCopier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.progressBarCopy = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxProgress = new System.Windows.Forms.TextBox();
            this.buttonSelectNickName = new System.Windows.Forms.Button();
            this.buttonBackup = new System.Windows.Forms.Button();
            this.labelSelectedNickNameValue = new System.Windows.Forms.Label();
            this.textBoxFocusser = new System.Windows.Forms.TextBox();
            this.labelProgressPercentage = new System.Windows.Forms.Label();
            this.buttonRestore = new System.Windows.Forms.Button();
            this.checkBoxIncludeUserConfig = new System.Windows.Forms.CheckBox();
            this.buttonAddToDesktop = new System.Windows.Forms.Button();
            this.buttonAddToStartup = new System.Windows.Forms.Button();
            this.buttonOpenConfigInspector = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Current NickName : ";
            // 
            // progressBarCopy
            // 
            this.progressBarCopy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarCopy.Location = new System.Drawing.Point(86, 436);
            this.progressBarCopy.Name = "progressBarCopy";
            this.progressBarCopy.Size = new System.Drawing.Size(600, 20);
            this.progressBarCopy.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 440);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Progress : ";
            // 
            // textBoxProgress
            // 
            this.textBoxProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxProgress.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxProgress.ForeColor = System.Drawing.SystemColors.InfoText;
            this.textBoxProgress.Location = new System.Drawing.Point(15, 91);
            this.textBoxProgress.Multiline = true;
            this.textBoxProgress.Name = "textBoxProgress";
            this.textBoxProgress.ReadOnly = true;
            this.textBoxProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxProgress.Size = new System.Drawing.Size(734, 327);
            this.textBoxProgress.TabIndex = 5;
            // 
            // buttonSelectNickName
            // 
            this.buttonSelectNickName.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonSelectNickName.Enabled = false;
            this.buttonSelectNickName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectNickName.Location = new System.Drawing.Point(15, 47);
            this.buttonSelectNickName.Name = "buttonSelectNickName";
            this.buttonSelectNickName.Size = new System.Drawing.Size(149, 25);
            this.buttonSelectNickName.TabIndex = 6;
            this.buttonSelectNickName.Text = "Select other NickName";
            this.buttonSelectNickName.UseVisualStyleBackColor = true;
            this.buttonSelectNickName.Click += new System.EventHandler(this.buttonSelectNickName_Click);
            // 
            // buttonBackup
            // 
            this.buttonBackup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBackup.Location = new System.Drawing.Point(170, 47);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Size = new System.Drawing.Size(71, 25);
            this.buttonBackup.TabIndex = 7;
            this.buttonBackup.Text = "Backup";
            this.buttonBackup.UseVisualStyleBackColor = true;
            this.buttonBackup.Click += new System.EventHandler(this.buttonBackup_Click);
            // 
            // labelSelectedNickNameValue
            // 
            this.labelSelectedNickNameValue.AutoSize = true;
            this.labelSelectedNickNameValue.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labelSelectedNickNameValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelectedNickNameValue.ForeColor = System.Drawing.SystemColors.Info;
            this.labelSelectedNickNameValue.Location = new System.Drawing.Point(149, 14);
            this.labelSelectedNickNameValue.Name = "labelSelectedNickNameValue";
            this.labelSelectedNickNameValue.Size = new System.Drawing.Size(108, 20);
            this.labelSelectedNickNameValue.TabIndex = 8;
            this.labelSelectedNickNameValue.Text = "No selection";
            // 
            // textBoxFocusser
            // 
            this.textBoxFocusser.Location = new System.Drawing.Point(-1, 47);
            this.textBoxFocusser.Name = "textBoxFocusser";
            this.textBoxFocusser.Size = new System.Drawing.Size(10, 20);
            this.textBoxFocusser.TabIndex = 1;
            this.textBoxFocusser.Visible = false;
            // 
            // labelProgressPercentage
            // 
            this.labelProgressPercentage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProgressPercentage.AutoSize = true;
            this.labelProgressPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProgressPercentage.Location = new System.Drawing.Point(705, 440);
            this.labelProgressPercentage.Name = "labelProgressPercentage";
            this.labelProgressPercentage.Size = new System.Drawing.Size(27, 13);
            this.labelProgressPercentage.TabIndex = 9;
            this.labelProgressPercentage.Text = "0 %";
            // 
            // buttonRestore
            // 
            this.buttonRestore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonRestore.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRestore.Location = new System.Drawing.Point(247, 47);
            this.buttonRestore.Name = "buttonRestore";
            this.buttonRestore.Size = new System.Drawing.Size(71, 25);
            this.buttonRestore.TabIndex = 10;
            this.buttonRestore.Text = "Restore";
            this.buttonRestore.UseVisualStyleBackColor = true;
            this.buttonRestore.Click += new System.EventHandler(this.buttonRestore_Click);
            // 
            // checkBoxIncludeUserConfig
            // 
            this.checkBoxIncludeUserConfig.AutoSize = true;
            this.checkBoxIncludeUserConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxIncludeUserConfig.Location = new System.Drawing.Point(330, 52);
            this.checkBoxIncludeUserConfig.Name = "checkBoxIncludeUserConfig";
            this.checkBoxIncludeUserConfig.Size = new System.Drawing.Size(188, 17);
            this.checkBoxIncludeUserConfig.TabIndex = 11;
            this.checkBoxIncludeUserConfig.Text = "Include userconfig in restore";
            this.checkBoxIncludeUserConfig.UseVisualStyleBackColor = true;
            this.checkBoxIncludeUserConfig.CheckedChanged += new System.EventHandler(this.checkBoxIncludeUserConfig_CheckedChanged);
            // 
            // buttonAddToDesktop
            // 
            this.buttonAddToDesktop.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonAddToDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddToDesktop.Location = new System.Drawing.Point(646, 14);
            this.buttonAddToDesktop.Name = "buttonAddToDesktop";
            this.buttonAddToDesktop.Size = new System.Drawing.Size(107, 27);
            this.buttonAddToDesktop.TabIndex = 13;
            this.buttonAddToDesktop.Text = "Add to Desktop";
            this.buttonAddToDesktop.UseVisualStyleBackColor = true;
            this.buttonAddToDesktop.Visible = false;
            this.buttonAddToDesktop.Click += new System.EventHandler(this.buttonAddToDesktop_Click);
            // 
            // buttonAddToStartup
            // 
            this.buttonAddToStartup.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonAddToStartup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddToStartup.Location = new System.Drawing.Point(529, 14);
            this.buttonAddToStartup.Name = "buttonAddToStartup";
            this.buttonAddToStartup.Size = new System.Drawing.Size(107, 27);
            this.buttonAddToStartup.TabIndex = 12;
            this.buttonAddToStartup.Text = "Add to Startup";
            this.buttonAddToStartup.UseVisualStyleBackColor = true;
            this.buttonAddToStartup.Visible = false;
            this.buttonAddToStartup.Click += new System.EventHandler(this.buttonAddToStartup_Click);
            // 
            // buttonOpenConfigInspector
            // 
            this.buttonOpenConfigInspector.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonOpenConfigInspector.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenConfigInspector.Location = new System.Drawing.Point(529, 44);
            this.buttonOpenConfigInspector.Name = "buttonOpenConfigInspector";
            this.buttonOpenConfigInspector.Size = new System.Drawing.Size(224, 25);
            this.buttonOpenConfigInspector.TabIndex = 14;
            this.buttonOpenConfigInspector.Text = "Open config inspector";
            this.buttonOpenConfigInspector.UseVisualStyleBackColor = true;
            this.buttonOpenConfigInspector.Click += new System.EventHandler(this.buttonOpenConfigInspector_Click);
            // 
            // FormAutomaticConfigCopier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.ClientSize = new System.Drawing.Size(761, 466);
            this.Controls.Add(this.buttonOpenConfigInspector);
            this.Controls.Add(this.buttonAddToDesktop);
            this.Controls.Add(this.buttonAddToStartup);
            this.Controls.Add(this.checkBoxIncludeUserConfig);
            this.Controls.Add(this.buttonRestore);
            this.Controls.Add(this.labelProgressPercentage);
            this.Controls.Add(this.textBoxFocusser);
            this.Controls.Add(this.labelSelectedNickNameValue);
            this.Controls.Add(this.buttonBackup);
            this.Controls.Add(this.buttonSelectNickName);
            this.Controls.Add(this.textBoxProgress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBarCopy);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "FormAutomaticConfigCopier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Config Copier";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAutomaticConfigCopier_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormAutomaticConfigCopier_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBarCopy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxProgress;
        private System.Windows.Forms.Button buttonSelectNickName;
        private System.Windows.Forms.Button buttonBackup;
        private System.Windows.Forms.Label labelSelectedNickNameValue;
        private System.Windows.Forms.TextBox textBoxFocusser;
        private System.Windows.Forms.Label labelProgressPercentage;
        private System.Windows.Forms.Button buttonRestore;
        private System.Windows.Forms.CheckBox checkBoxIncludeUserConfig;
        private System.Windows.Forms.Button buttonAddToDesktop;
        private System.Windows.Forms.Button buttonAddToStartup;
        private System.Windows.Forms.Button buttonOpenConfigInspector;

    }
}