﻿namespace ConfigCopier
{
    partial class FormConfigCopier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.textBoxSourceFile = new System.Windows.Forms.TextBox();
            this.textBoxDestinationDirectory = new System.Windows.Forms.TextBox();
            this.checkBoxBackupExsisting = new System.Windows.Forms.CheckBox();
            this.labelSource = new System.Windows.Forms.Label();
            this.labelDestination = new System.Windows.Forms.Label();
            this.comboBoxNickName = new System.Windows.Forms.ComboBox();
            this.labelNickName = new System.Windows.Forms.Label();
            this.openFileDialogSourceFile = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogDestinationDir = new System.Windows.Forms.OpenFileDialog();
            this.buttonOpenSourceFileDialog = new System.Windows.Forms.Button();
            this.buttonOpenDestinationDir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(244, 160);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonCopy
            // 
            this.buttonCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCopy.Location = new System.Drawing.Point(325, 160);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(75, 23);
            this.buttonCopy.TabIndex = 1;
            this.buttonCopy.Text = "Copy";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // textBoxSourceFile
            // 
            this.textBoxSourceFile.Location = new System.Drawing.Point(12, 65);
            this.textBoxSourceFile.Name = "textBoxSourceFile";
            this.textBoxSourceFile.Size = new System.Drawing.Size(354, 20);
            this.textBoxSourceFile.TabIndex = 2;
            // 
            // textBoxDestinationDirectory
            // 
            this.textBoxDestinationDirectory.Location = new System.Drawing.Point(12, 104);
            this.textBoxDestinationDirectory.Name = "textBoxDestinationDirectory";
            this.textBoxDestinationDirectory.Size = new System.Drawing.Size(354, 20);
            this.textBoxDestinationDirectory.TabIndex = 3;
            // 
            // checkBoxBackupExsisting
            // 
            this.checkBoxBackupExsisting.AutoSize = true;
            this.checkBoxBackupExsisting.Checked = true;
            this.checkBoxBackupExsisting.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxBackupExsisting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxBackupExsisting.Location = new System.Drawing.Point(12, 130);
            this.checkBoxBackupExsisting.Name = "checkBoxBackupExsisting";
            this.checkBoxBackupExsisting.Size = new System.Drawing.Size(155, 17);
            this.checkBoxBackupExsisting.TabIndex = 4;
            this.checkBoxBackupExsisting.Text = "Backup existing config";
            this.checkBoxBackupExsisting.UseVisualStyleBackColor = true;
            // 
            // labelSource
            // 
            this.labelSource.AutoSize = true;
            this.labelSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSource.Location = new System.Drawing.Point(13, 49);
            this.labelSource.Name = "labelSource";
            this.labelSource.Size = new System.Drawing.Size(68, 13);
            this.labelSource.TabIndex = 5;
            this.labelSource.Text = "Source file";
            // 
            // labelDestination
            // 
            this.labelDestination.AutoSize = true;
            this.labelDestination.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDestination.Location = new System.Drawing.Point(12, 88);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(124, 13);
            this.labelDestination.TabIndex = 6;
            this.labelDestination.Text = "Destination directory";
            // 
            // comboBoxNickName
            // 
            this.comboBoxNickName.FormattingEnabled = true;
            this.comboBoxNickName.Location = new System.Drawing.Point(12, 25);
            this.comboBoxNickName.Name = "comboBoxNickName";
            this.comboBoxNickName.Size = new System.Drawing.Size(388, 21);
            this.comboBoxNickName.TabIndex = 7;
            // 
            // labelNickName
            // 
            this.labelNickName.AutoSize = true;
            this.labelNickName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNickName.Location = new System.Drawing.Point(12, 9);
            this.labelNickName.Name = "labelNickName";
            this.labelNickName.Size = new System.Drawing.Size(65, 13);
            this.labelNickName.TabIndex = 8;
            this.labelNickName.Text = "NickName";
            // 
            // openFileDialogSourceFile
            // 
            this.openFileDialogSourceFile.DefaultExt = "*.cfg";
            this.openFileDialogSourceFile.FileName = "Config.cfg";
            this.openFileDialogSourceFile.Filter = "Configuration files|*.cfg|All|*.*";
            this.openFileDialogSourceFile.InitialDirectory = "c:\\";
            this.openFileDialogSourceFile.Title = "Select your config file";
            // 
            // openFileDialogDestinationDir
            // 
            this.openFileDialogDestinationDir.CheckFileExists = false;
            this.openFileDialogDestinationDir.InitialDirectory = "c:\\";
            this.openFileDialogDestinationDir.SupportMultiDottedExtensions = true;
            this.openFileDialogDestinationDir.Title = "Select destination directory";
            // 
            // buttonOpenSourceFileDialog
            // 
            this.buttonOpenSourceFileDialog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenSourceFileDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenSourceFileDialog.Location = new System.Drawing.Point(372, 65);
            this.buttonOpenSourceFileDialog.Name = "buttonOpenSourceFileDialog";
            this.buttonOpenSourceFileDialog.Size = new System.Drawing.Size(28, 23);
            this.buttonOpenSourceFileDialog.TabIndex = 9;
            this.buttonOpenSourceFileDialog.Text = "...";
            this.buttonOpenSourceFileDialog.UseVisualStyleBackColor = true;
            this.buttonOpenSourceFileDialog.Click += new System.EventHandler(this.buttonOpenSourceFileDialog_Click);
            // 
            // buttonOpenDestinationDir
            // 
            this.buttonOpenDestinationDir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenDestinationDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenDestinationDir.Location = new System.Drawing.Point(372, 101);
            this.buttonOpenDestinationDir.Name = "buttonOpenDestinationDir";
            this.buttonOpenDestinationDir.Size = new System.Drawing.Size(28, 23);
            this.buttonOpenDestinationDir.TabIndex = 10;
            this.buttonOpenDestinationDir.Text = "...";
            this.buttonOpenDestinationDir.UseVisualStyleBackColor = true;
            this.buttonOpenDestinationDir.Click += new System.EventHandler(this.buttonOpenDestinationDir_Click);
            // 
            // FormConfigCopier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 192);
            this.Controls.Add(this.buttonOpenDestinationDir);
            this.Controls.Add(this.buttonOpenSourceFileDialog);
            this.Controls.Add(this.labelNickName);
            this.Controls.Add(this.comboBoxNickName);
            this.Controls.Add(this.labelDestination);
            this.Controls.Add(this.labelSource);
            this.Controls.Add(this.checkBoxBackupExsisting);
            this.Controls.Add(this.textBoxDestinationDirectory);
            this.Controls.Add(this.textBoxSourceFile);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.buttonCancel);
            this.Name = "FormConfigCopier";
            this.Text = "Config copier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.TextBox textBoxSourceFile;
        private System.Windows.Forms.TextBox textBoxDestinationDirectory;
        private System.Windows.Forms.CheckBox checkBoxBackupExsisting;
        private System.Windows.Forms.Label labelSource;
        private System.Windows.Forms.Label labelDestination;
        private System.Windows.Forms.ComboBox comboBoxNickName;
        private System.Windows.Forms.Label labelNickName;
        private System.Windows.Forms.OpenFileDialog openFileDialogSourceFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogDestinationDir;
        private System.Windows.Forms.Button buttonOpenSourceFileDialog;
        private System.Windows.Forms.Button buttonOpenDestinationDir;

    }
}

