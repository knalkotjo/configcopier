﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using ConfigCopier.Forms;
using ConfigCopier.Models;
using TrayApp;

namespace ConfigCopier
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool ownsMutex;

            using (Mutex mutex = new Mutex(true, "MutexConfigCopier", out ownsMutex))
            {
                if (ownsMutex)
                {
                    //Console.WriteLine("Owned");

                    using (MyTrayIconContainer objTrayIconContainer = new MyTrayIconContainer())
                    {
                        // Run the application with the specific context. 
                        // It will exit when defined in the context
                        if (!objTrayIconContainer.CriticalError)
                        {
                            Application.Run(objTrayIconContainer);
                        }
                    }

                    //Application.EnableVisualStyles();
                    //Application.SetCompatibleTextRenderingDefault(false);

                    //try
                    //{
                    //    FormSelectNick _MainForm = new FormSelectNick();
                    //    //FormConfigInspector _MainForm = new FormConfigInspector();
                    //    //FormAutomaticConfigCopier _MainForm = new FormAutomaticConfigCopier();
                    //    //Application.Run(new FormConfigCopier());
                    //    Application.Run(_MainForm);
                    //}
                    //catch (ApplicationException e)
                    //{
                    //    //Exit                
                    //}
                    
                    mutex.ReleaseMutex();
                }
                else
                {
                    //exit
                    //Console.WriteLine("Another instance of this application " +
                    //    " already owns the mutex named MutexExample.");
                }
            }

        }
    }
}
