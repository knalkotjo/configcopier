﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConfigCopier.Models
{
    public class NickDataModel
    {
        // Members in coonstructor
        public String NickName { get; set; } // == gamekot 'name' (cs,lol, ...
//        public String FirstName { get; set; }
//        public String LastName { get; set; }
        public decimal Killer { get; set; }
        public decimal AidPoints { get; set; }
        public int UserId { get; set; }
        public String SteamName { get; set; }
        //public int Uid { get; set; }


        // members not in constructor !
        private string _Ip;
        private string _PcName;

        // game user id's : to be filled correctly here in combination with the mySql query data!
        public string GameIdCs { get; set; }
        public string GameIdLol { get; set; }
        public string GameIdWorldOfTanks { get; set; }

        public NickDataModel() { }

        public NickDataModel(String nickName, String steamName,String firstName, String lastName, int pcId,decimal killer,decimal aidPoints)
        {
            NickName = nickName;
            //FirstName = firstName;
            //LastName = lastName;
            UserId = pcId;
            Killer = killer;
            AidPoints = aidPoints;
            SteamName = steamName;

            GameIdCs = nickName;
            GameIdWorldOfTanks = nickName;
            GameIdLol = nickName;
        }

        public void SetPcName(string pcName)
        {
            _PcName = pcName;
        }
        public string GetPcName()
        {
            return _PcName;
        }

        public void SetIp(string ip)
        {
            _Ip = ip;
        }
        public string GetIp()
        {
            return _Ip;
        }

    }
}
