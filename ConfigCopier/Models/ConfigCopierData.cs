﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ConfigCopier.Models
{
    class ConfigCopierData
    {
        // these are default values, actual values from app.config
        public String _LocalConfigPath = "";
        public String _ServerConfigPath = "";
        public String _HalfLifeConfigPath = "";

        public String _LocalConfigPathTest = "";
        public String _ServerConfigPathTest = "";
        public String _HalfLifeConfigPathTest = "";

        public String _FileNameConfig = "config";
        public String _FileNameUserConfig = "userconfig";
        public String _FileNameExt = ".cfg";

        public void ReadConfigSettings()
        {
            foreach (string key in ConfigurationSettings.AppSettings)
            {
                string val = ConfigurationSettings.AppSettings[key.ToString()].ToString();
                switch (key.ToUpper())
                {
                    case "FILENAMECONFIG":
                        {
                            _FileNameConfig = val;
                        }
                        break;
                    case "FILENAMEUSERCONFIG":
                        {
                            _FileNameUserConfig = val;
                        }
                        break;
                    case "FILENAMEEXT":
                        {
                            _FileNameExt = val;
                        }
                        break;
                    case "LOCALCONFIGPATH":
                        {
                            _LocalConfigPath = val;
                        }
                        break;
                    case "SERVERCONFIGPATH":
                        {
                            _ServerConfigPath = val;
                        }
                        break;
                    case "HALFLIFECONFIGPATH":
                        {
                            _HalfLifeConfigPath = val;
                        }
                        break;
                    case "LOCALCONFIGPATHTEST":
                        {
                            _LocalConfigPathTest = val;
                        }
                        break;
                    case "SERVERCONFIGPATHTEST":
                        {
                            _ServerConfigPathTest = val;
                        }
                        break;
                    case "HALFLIFECONFIGPATHTEST":
                        {
                            _HalfLifeConfigPathTest = val;
                        }
                        break;
                }
            }
        }

    }
}
