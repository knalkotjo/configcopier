﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConfigCopier.Models
{
    class FileCopyInfo
    {
        public String Source { get; set; }
        public String Destination { get; set; }
        public bool Overwrite { get; set; }

        public String NotValidErrorMessage{ get; set; }

        public FileCopyInfo(String source,String destination,bool overwrite)
        {
            Source = source;
            Destination = destination;
            Overwrite = overwrite;
            NotValidErrorMessage = "";
        }

        public bool IsValid()
        {
            if (String.IsNullOrEmpty(Source))
            {
                NotValidErrorMessage = "Source not specified";
                return false;
            }

            if (String.IsNullOrEmpty(Destination))
            {
                NotValidErrorMessage = "Destination not specified";
                return false;
            }
            if (!File.Exists(Source))
            {
                NotValidErrorMessage = "Source does not exist : [" + Source + "]";
                return false;
            }

            if (!File.Exists(Destination))
            {
                NotValidErrorMessage = "Destination does not exist : [" + Destination + "]";
                return false;
            }
            
            return true;
        }
    }
}
