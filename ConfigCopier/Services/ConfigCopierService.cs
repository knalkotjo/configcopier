﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ConfigCopier.Services

{
    public static class ConfigCopierService
    {
        public static bool TestSqlServerConnection(string currentCallingFormTitle)
        {
            var connectionString = ConfigurationManager.AppSettings["MasterReader.ConnectionString"];

            MySqlConnection myConnection = new MySqlConnection();
            myConnection.ConnectionString = connectionString;

            var xx = myConnection.ConnectionTimeout;

            try
            {
                myConnection.Open();
                // anything else ?
                //currentCallingFormTitle = currentCallingFormTitle + " SQL Connection ok !";
                
                myConnection.Close();
            }
            catch (Exception e)
            {
                //currentCallingFormTitle = currentCallingFormTitle + " SQL Connection NOK !";
                
                //they don't want no messages :(
                //MessageBox.Show(e.Message);
                if (myConnection != null)
                {
                    myConnection.Close();
                }
                return false;
            }
            return true;
        }

        /*
        *   return true  : dir created
        *   return false : dir not created (allready exists)
        */
        public static bool CheckCreateDir(String path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
                return true;
            }
            return false;
        }

        public static bool CheckCreateDir(string path, Action<string> addProgressLine)
        {
            addProgressLine("check if exists from del");
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
                addProgressLine("dir created from del");
                return true;
            }
            return false;
        }

        public static bool CheckDirectoryPath(String path)
        {
            try
            {
                if (System.IO.Directory.Exists(path))
                {
                    return true;
                }
                return false;

            }
            catch (Exception)
            {
                return false;
            }
        }

        //public static void SaveLoginData(int userId,DateTime startDateTime,DateTime endDateTime,string ip)
        //{
            
        //    //INSERT INTO usrlog('usr_id' ,'datetime_in' ,'last_datetime' ,'ip')
        //    //VALUES ('2', '2011-xx-xx 00:00:00', '2011-08-27 00:00:00', '192.168.1.123')            
        //}

        public static String GetConfigFileNickName(string fullFileName)
        {
            String result = "";

            try
            {
                StreamReader reader = new StreamReader(fullFileName);
                string line;
                bool bFound = false;
                while ((line = reader.ReadLine()) != null && !bFound)
                {
                    if (line.Contains("name"))
                    {
                        if (line.Substring(0, 4) == "name")
                        {
                            var arr = line.Split(' ');
                            result = arr[1];
                            result = result.Replace("\"", "");
                            bFound = true;
                        }
                    }
                }

                if (!bFound)
                {
                    result = "no cs name in file";
                }
            }
            catch (Exception)
            {
                result = "no config file";
                // logging entry ?
                //MessageBox.Show("file does not exsist !");
                //throw;
            }
            return result;
        }

        public static List<Models.NickDataModel> GetNickListTestData()
        {
            var nickList = new List<Models.NickDataModel>();

            nickList.Add(new Models.NickDataModel("PainKiller","", "Jo", "Schoenmakers", 64,0,0));
            nickList.Add(new Models.NickDataModel("m0l0l0v", "", "Bert", "Vanderhallen", 2, 0, 0));
            nickList.Add(new Models.NickDataModel("BaRaBaS", "", "Bart", "", 31, 0, 0));
            nickList.Add(new Models.NickDataModel("bL@CkOuT", "", "Bram", "", 71, 0, 0));
            nickList.Add(new Models.NickDataModel("BRO", "", "", "", 95, 0, 0));
            nickList.Add(new Models.NickDataModel("LuciFer", "", "", "", 100, 0, 0));
            nickList.Add(new Models.NickDataModel("Allahsnackbar", "", "", "", 5, 0, 0));
            nickList.Add(new Models.NickDataModel("VuileKameel", "", "", "", 22, 0, 0));
            nickList.Add(new Models.NickDataModel("Alva", "", "", "", 55, 0, 0));
            nickList.Add(new Models.NickDataModel("LoneWulf", "", "", "", 82, 0, 0));
            nickList.Add(new Models.NickDataModel("FullMoon", "", "", "", 107, 0, 0));
            nickList.Add(new Models.NickDataModel("polle pedal", "", "", "", 32, 0, 0));
            nickList.Add(new Models.NickDataModel("Carpinteir0", "", "", "", 74, 0, 0));
            nickList.Add(new Models.NickDataModel("St.Phano", "", "", "", 90, 0, 0));
            nickList.Add(new Models.NickDataModel("FuSe", "", "", "", 80, 0, 0));

            return nickList;
        }

        //public static void getFileViaFtp(string server, string user, string password, string directoryName, string origin, string destination)
        //{
        //    //var ftpPath = ConfigurationManager.AppSettings["ftpPath"];
        //    var ftpClient = new ConfigCopier.Services.FtpService(server, user, password);
        //    //var ftpOrigin = ftpPath + "/" + nick.UserId + "/";

        //    ftpClient.DownloadFile(origin, destination);

        //}

        public static int getDirFilesViaFtp(string server, string user, string password, string directoryName, string localConfigDir,string filter)
        {
            var ftpClient = new ConfigCopier.Services.FtpService(server, user, password);
            //var ftpOrigin = ftpPath + "/" + nick.UserId + "/";
            var fileList = ftpClient.DirectoryListing(directoryName);

            // gets 64/filename ??
            bool filterActivated = !String.IsNullOrEmpty(filter);
            int cnt = 0;
            foreach(string fileNameWithDir in fileList)
            {
                if (filterActivated && fileNameWithDir.Contains(filter))
                {
                    var arr = fileNameWithDir.Split('/');
                    var dir = arr[0];
                    var fileName = arr[1];
                    string remoteSource = fileNameWithDir;//directoryName + "/" + fileName;
                    string localDestination = localConfigDir + "/" + fileName;

                    ftpClient.DownloadFile(remoteSource, localDestination);
                    cnt++;
                }
            }

            return cnt;
        }

        public static List<String> GetFtpDirFileList(string server, string user, string password, string directoryName) 
        {
            var ftpClient = new ConfigCopier.Services.FtpService(server, user, password);
            return ftpClient.DirectoryListing(directoryName);
        }

        // mount encrypted admin partition : todo : make hidden admin caller
        public static void MountEncryptedPartition()
        {
            /*
             cmd
            cd
            C:\Program Files\TrueCrypt
            truecrypt /v d:\knalkot /le /a /p L0lencryptedknalkot /e
             */

            // key up event is not captured because of the message box
            //_FormK_Pressed = false;
            //MessageBox.Show("Yipikaye MF");

            string cmd = @"C:\Program Files\TrueCrypt\truecrypt.exe";
            string param = @"/v d:\knalkot /le /a /p L0lencryptedknalkot /e";

            SchumiLibrary.Services.SystemService.ExecuteCommand(cmd, param);

        }

    }
}
