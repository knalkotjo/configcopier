﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Permissions;
using System.Windows.Forms;
//using Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;

[assembly: RegistryPermissionAttribute(SecurityAction.RequestMinimum, All = "HKEY_CURRENT_USER")]

namespace ConfigCopier.Services
{
    static class TrayService
    {
        //public static string GetProxyExceptions()
        //{
        //    string text = "";
        //    text = "under construction";

        //    string regKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
        //    string regName = "ProxyOverride";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey);
        //    text = rkTest.GetValue(regName).ToString();
        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    // Open the test key in read/write mode.
        //    //rkTest = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("RegistryOpenSubKeyExample", true);
        //    //rkTest.SetValue("TestName", "TestValue");
        //    //Console.WriteLine("Test value for TestName: {0}", rkTest.GetValue("TestName"));
        //    //rkTest.Close();

        //    return text;
        //}

        //public static bool SetProxyExceptions(string text)
        //{

        //    string regKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
        //    string regName = "ProxyOverride";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey,true);
        //    rkTest.SetValue(regName, text);

        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    // Open the test key in read/write mode.
        //    //rkTest = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("RegistryOpenSubKeyExample", true);
        //    //rkTest.SetValue("TestName", "TestValue");
        //    //Console.WriteLine("Test value for TestName: {0}", rkTest.GetValue("TestName"));
        //    //rkTest.Close();

        //    return true;
        //}

        //public static bool ExecuteCommand(String fileName, String arguments)
        //{
        //    Process proc = new System.Diagnostics.Process();
        //    proc.EnableRaisingEvents = false;
        //    proc.StartInfo.FileName = fileName;
        //    proc.StartInfo.Arguments = arguments;
        //    return proc.Start();
        //}

        //public static bool ExecuteCommandAndWait(String fileName, String arguments)
        //{
        //    Process proc = new System.Diagnostics.Process();
        //    proc.EnableRaisingEvents = false;
        //    proc.StartInfo.FileName = fileName;
        //    proc.StartInfo.Arguments = arguments;
        //    proc.Start();
        //    proc.WaitForExit();

        //    return true;
        //}

        //public static string GenerateDateTimeString()
        //{
        //    var today = DateTime.Today;

        //    String fileName = "";
        //    fileName += today.ToString("yyyyMMdd");
        //    fileName += "_";
        //    fileName += today.ToString("mmss");

        //    //var random = new Random();
        //    //fileName += "_";
        //    //fileName += random.Next(100000, 999999).ToString();

        //    return fileName;
        //}

        //public static void SetToolTip(object control,string toolTipText, bool showAlways)
        //{
        //    // Create the ToolTip and associate with the Form container.
        //    ToolTip toolTip1 = new ToolTip();

        //    // Set up the delays for the ToolTip.
        //    toolTip1.AutoPopDelay = 5000;
        //    toolTip1.InitialDelay = 1000;
        //    toolTip1.ReshowDelay = 500;
        //    // Force the ToolTip text to be displayed whether or not the form is active.
        //    if (showAlways)
        //    {
        //        toolTip1.ShowAlways = true;
        //    }

        //    // Set up the ToolTip text for the Button and Checkbox.
        //    toolTip1.SetToolTip((Control)control, toolTipText);
        //}

        //public static bool GetProxyEnabled()
        //{
        //    bool val = false;
        //    string strVal = "0";
        //    string regKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
        //    string regName = "ProxyEnable";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey);
        //    strVal = rkTest.GetValue(regName).ToString();
        //    if(strVal == "1")
        //    {
        //        val = true;
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    // Open the test key in read/write mode.
        //    //rkTest = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("RegistryOpenSubKeyExample", true);
        //    //rkTest.SetValue("TestName", "TestValue");
        //    //Console.WriteLine("Test value for TestName: {0}", rkTest.GetValue("TestName"));
        //    //rkTest.Close();

        //    return val;
        //}

        //public static void SetProxyEnabled(bool value)
        //{

        //    string regKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings";
        //    string regName = "ProxyEnable";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey, true);
        //    if(value)
        //    {
        //        rkTest.SetValue(regName, 1);
        //    }
        //    else
        //    {
        //        rkTest.SetValue(regName, 0);
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    // Open the test key in read/write mode.
        //    //rkTest = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("RegistryOpenSubKeyExample", true);
        //    //rkTest.SetValue("TestName", "TestValue");
        //    //Console.WriteLine("Test value for TestName: {0}", rkTest.GetValue("TestName"));
        //    //rkTest.Close();

        //}

        //public static bool GetScriptDebuggerIEDisabled()
        //{
        //    bool val = false;
        //    string strVal = "no";
        //    string regKey = "Software\\Microsoft\\Internet Explorer\\Main";
        //    string regName = "DisableScriptDebuggerIE";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey);
        //    strVal = rkTest.GetValue(regName).ToString();
        //    if (strVal == "yes")
        //    {
        //        val = true;
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    return val;
        //}

        //public static void SetScriptDebuggerIEDisabled(bool value)
        //{
        //    string regKey = "Software\\Microsoft\\Internet Explorer\\Main";
        //    string regName = "DisableScriptDebuggerIE";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey, true);
        //    if (value)
        //    {
        //        rkTest.SetValue(regName, "yes");
        //    }
        //    else
        //    {
        //        rkTest.SetValue(regName, "no");
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();
        //}

        //public static bool GetScriptDebuggerDisabled()
        //{
        //    bool val = false;
        //    string strVal = "no";
        //    string regKey = "Software\\Microsoft\\Internet Explorer\\Main";
        //    string regName = "Disable Script Debugger";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey);
        //    strVal = rkTest.GetValue(regName).ToString();
        //    if (strVal == "yes")
        //    {
        //        val = true;
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();

        //    return val;
        //}

        //public static void SetScriptDebuggerDisabled(bool value)
        //{
        //    string regKey = "Software\\Microsoft\\Internet Explorer\\Main";
        //    string regName = "Disable Script Debugger";

        //    Microsoft.Win32.RegistryKey rkCurrentUser = Microsoft.Win32.Registry.CurrentUser;

        //    Microsoft.Win32.RegistryKey rkTest = rkCurrentUser.OpenSubKey(regKey, true);
        //    if (value)
        //    {
        //        rkTest.SetValue(regName, "yes");
        //    }
        //    else
        //    {
        //        rkTest.SetValue(regName, "no");
        //    }

        //    rkTest.Close();
        //    rkCurrentUser.Close();
        //}

        //public static void SendNewMail()
        //{
        //    // Create an Outlook Application object. 
        //    //Application outLookApp = new Application();
        //    ApplicationClass outLookApp = new ApplicationClass();
            
        //    // Create a new MailItem.
        //    MailItem myMail = (MailItem)outLookApp.CreateItem(OlItemType.olMailItem);
        //    myMail.Recipients.Add("jo.schoenmakers@macadam.eu");
        //    myMail.Subject = "This is the winter of our discontent";
        //    myMail.Body = "Indeed !!";
        //    // Send it!
        //    myMail.Send();

        //    //// Explicitly destroy COM type after use.
        //    //Marshal.ReleaseComObject(c);
        
        //}
        
    }
}
