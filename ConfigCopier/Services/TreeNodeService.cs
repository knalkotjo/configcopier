﻿
using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace ConfigCopier.Services
{

    public static class TreeNodeService
    {

        public static void PopulateDirectory(TreeNode nodeCurrent,TreeNodeCollection nodeCurrentCollection) 
{ 
    TreeNode nodeDir; 
    int imageIndex = 2; 
    int selectIndex = 3;
    
    if (nodeCurrent.SelectedImageIndex != 0) 
    {    //populate treeview with folders 

        try 
        { 
            if(Directory.Exists(getFullPath(nodeCurrent.FullPath)) 
                == false) 
            { 
                MessageBox.Show("Directory or path " + 
                    nodeCurrent.ToString() + " does not exist."); 
            } 
            else 
            {    //populate files 

                PopulateFiles(nodeCurrent);
                
                string[] stringDirectories = 
                    Directory.GetDirectories(getFullPath
                        (nodeCurrent.FullPath)); 
                
                string stringFullPath = ""; 
                string stringPathName = ""; 
                
                foreach (string stringDir in stringDirectories) 
                { 
                    stringFullPath = stringDir; 
                    stringPathName = GetPathName(stringFullPath); 
                    
                    //create node for directories 

                    nodeDir = new TreeNode(stringPathName.ToString(),
                        imageIndex,selectIndex); 
            
                    nodeCurrentCollection.Add(nodeDir); 
                } 
            } 
        } 
        catch (IOException e) 
        { 
            MessageBox.Show("Error: Drive not ready or directory does not exist."); 
        } 
        catch (UnauthorizedAccessException e) 
        { 
            MessageBox.Show("Error: Drive or directory access denided."); 
        } 
        catch (Exception e) 
        { 
            MessageBox.Show("Error: " + e); 
        } 
    } 
} 

        public static string GetPathName(string stringPath) 
{ 
    //Get Name of folder 

    string[] stringSplit = stringPath.Split('\\'); 
    
    int _maxIndex = stringSplit.Length; 
    
    return stringSplit[_maxIndex-1]; 
} 

        public static void PopulateFiles(TreeNode nodeCurrent)
{
    //Populate listview with files

    string[] lvData =  new string[4];
    
    //clear list

    //InitListView();

    if (nodeCurrent.SelectedImageIndex != 0) 
    {
        //check path

        if(Directory.Exists((string) 
            getFullPath(nodeCurrent.FullPath)) == false)
        {
            MessageBox.Show("Directory or path " + 
                nodeCurrent.ToString() + " does not exist.");
        }
        else
        {
            try
            {
                string[] stringFiles = Directory.GetFiles
                    (getFullPath(nodeCurrent.FullPath));
                string stringFileName = "";
                DateTime dtCreateDate, dtModifyDate;
                Int64 lFileSize = 0;

                //loop throught all files

                foreach (string stringFile in stringFiles)
                {
                    stringFileName = stringFile;
                    FileInfo objFileSize = new 
                        FileInfo(stringFileName);
                    lFileSize = objFileSize.Length;
                    //GetCreationTime(stringFileName);

                    dtCreateDate = objFileSize.CreationTime; 
                    //GetLastWriteTime(stringFileName);

                    dtModifyDate = objFileSize.LastWriteTime; 

                    //create listview data

                    lvData[0] = GetPathName(stringFileName);
                    lvData[1] = formatSize(lFileSize);
                            
                    //check if file is in local current 

                    //day light saving time

                    if (TimeZone.CurrentTimeZone.
                        IsDaylightSavingTime(dtCreateDate) == false)
                    {
                        //not in day light saving time adjust time

                        lvData[2] = formatDate(dtCreateDate.AddHours(1));
                    }
                    else
                    {
                        //is in day light saving time adjust time

                        lvData[2] = formatDate(dtCreateDate);
                    }

                    //check if file is in local current day 

                    //light saving time

                    if (TimeZone.CurrentTimeZone.
                        IsDaylightSavingTime(dtModifyDate) == false)
                    {
                        //not in day light saving time adjust time

                        lvData[3] = formatDate(dtModifyDate.AddHours(1));
                    }
                    else{
                        //not in day light saving time adjust time

                        lvData[3] = formatDate(dtModifyDate);
                    }

                    //Create actual list item

                    ListViewItem lvItem = new ListViewItem(lvData,0);
                    
                    //lvFiles.Items.Add(lvItem); // ??

                }
            }
            catch (IOException e)
            {
                MessageBox.Show("Error: Drive not ready or directory does not exist.");
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show("Error: Drive or directory access denided.");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error: " + e);
            }
        }
    }
}

        public static string getFullPath(string stringPath) 
{ 
    //Get Full path string 

    var stringParse = ""; 
    
    //remove My Computer from path. 

    stringParse = stringPath.Replace("My Computer\\", ""); 
    
    return stringParse; 
}

        public static string formatDate(DateTime dtDate) 
{ 
    //Get date and time in short format 
    string stringDate = ""; 
    stringDate = dtDate.ToShortDateString().ToString() 
        + " " + dtDate.ToShortTimeString().ToString();
    
    return stringDate; 
}

        public static string formatSize(Int64 lSize) 
{
    //Format number to KB 
    string stringSize = ""; 
    NumberFormatInfo myNfi = new NumberFormatInfo();
    Int64 lKBSize = 0; 
    
    if (lSize < 1024 ) 
    { 
        if (lSize == 0) 
        { 
            //zero byte 
            stringSize = "0"; 
        } 
        else 
        { 
            //less than 1K but not zero byte 
            stringSize = "1"; 
        } 
    } 
    else 
    { 
        //convert to KB 
        lKBSize = lSize / 1024; 
        
        //format number with default format 
        stringSize = lKBSize.ToString("n",myNfi); 
        
        //remove decimal 
        stringSize = stringSize.Replace(".00", ""); 
    } 
    
    return stringSize + " KB"; 
}
    }
}